﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Property Name="CCSymbols" Type="Str">CSPP_BuildContent,HB;DSCGetVarList,PSP;</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str">This LabVIEW project "CS++.lvproj" is used to develop the successor of the CS Framework.

- CS++ will be based on native LabVIEW classes and the Actor Framework.
- CS++ will follow the KISS principle: "Keep It Smart &amp; Simple"

Please refer also to README.txt.

Author: H.Brand@gsi.de, D.Neidherr@gsi.de

Copyright 2013  GSI Helmholtzzentrum für Schwerionenforschung GmbH

Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.</Property>
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Property Name="varPersistentID:{004FDCFF-57CD-4CD3-A9B4-BEC439EC7E28}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_PollingInterval</Property>
	<Property Name="varPersistentID:{03F95C17-1D18-4D4B-A338-3FC919AEDC04}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_TriggerCurrentLimit_2</Property>
	<Property Name="varPersistentID:{04049627-793E-48A4-AE11-A4E43D630F12}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Set-StopTrigger_0</Property>
	<Property Name="varPersistentID:{042561F7-6034-4636-8B0B-E8C608F25114}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{05CF2EF6-1CC1-4C2C-A268-9BF559B7C15A}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Set-ArbitraryMemory</Property>
	<Property Name="varPersistentID:{078D5178-A2DE-4DB4-B49F-25E3BB39B1C8}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_StopTriggerSource_0</Property>
	<Property Name="varPersistentID:{098B7719-60DB-42A2-8ACD-25954B5333A3}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_MeasurementDone</Property>
	<Property Name="varPersistentID:{0B45370F-4368-468C-AD79-630092A39B51}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_MPReading</Property>
	<Property Name="varPersistentID:{0BD1785F-1CD1-4506-85BF-D25C438FA3C8}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_ACLineTriggerSlope</Property>
	<Property Name="varPersistentID:{0C672D15-A9C1-4D8F-B568-2E9C3D92850F}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_AutoZeroMode</Property>
	<Property Name="varPersistentID:{0D19A555-BCCC-40D0-9AF7-C2D9E5D88362}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_Measure_V_2</Property>
	<Property Name="varPersistentID:{0D1F4D52-E921-479D-8CBF-89A6713503EA}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_CircuitMode_3</Property>
	<Property Name="varPersistentID:{0DDD5C52-A775-483F-A30A-F02BABA83A94}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_PollingIterations</Property>
	<Property Name="varPersistentID:{0ECD4ED7-6441-444D-B0CD-59BBB4E5CECD}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_Status_0</Property>
	<Property Name="varPersistentID:{0ECF0EA3-08CF-45CD-A093-1C1B33841FF3}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_ResourceName</Property>
	<Property Name="varPersistentID:{0F11E5B7-86F7-41CC-91CE-B6CB11FC97F9}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_TimePerRecord</Property>
	<Property Name="varPersistentID:{1007EA4C-9D20-4B58-995B-12E61063C527}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_VerticalCoupling_3</Property>
	<Property Name="varPersistentID:{1017F63E-55F6-49DE-95C3-9BDBC075C955}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/myDeviceActor_PollingDeltaT</Property>
	<Property Name="varPersistentID:{10AADD95-12F1-4448-A837-B698213A358C}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_SPS_0</Property>
	<Property Name="varPersistentID:{1157D41C-092A-422D-BE23-4BB5A18D3E6E}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/myBaseProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{117E4492-B32C-4450-9522-B36CC76EA289}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_DriverRevision</Property>
	<Property Name="varPersistentID:{130505F9-6E9A-4AC6-A228-00FB832CC934}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Set-SoftwareAdvancedTrigger</Property>
	<Property Name="varPersistentID:{14221CF6-6BD1-46F6-AB1E-88FD9D424297}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_NumberOfChannels</Property>
	<Property Name="varPersistentID:{171E3A84-08A3-49AB-BED5-E41CA5DA8F7A}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Set-SoftwareResumeTrigger</Property>
	<Property Name="varPersistentID:{1820E63C-34DD-4B77-9B41-511403A8664F}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_PollingIterations</Property>
	<Property Name="varPersistentID:{1832F212-57FA-405B-B754-B976CFB8B29F}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_FM-Enabled_0</Property>
	<Property Name="varPersistentID:{18619A63-9234-48B6-805E-8A4B343248F6}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_TriggerSource_0</Property>
	<Property Name="varPersistentID:{19AB9AA7-ABE6-484D-914E-B28C71C47256}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{1A1D617F-215F-4E68-8F81-08F2B20DBA53}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_FixedReferenceJunction</Property>
	<Property Name="varPersistentID:{1B0763AD-A88D-4CAC-834D-C6DFC3D4A81C}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_MaxVoltageLevel_0</Property>
	<Property Name="varPersistentID:{1B0F0B9C-0914-4A4C-9C6B-8626D7EF2E1E}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_MinRecordLength</Property>
	<Property Name="varPersistentID:{1B1043E9-E75C-498F-991C-C1D81EF73462}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_PollingMode</Property>
	<Property Name="varPersistentID:{1B1CE2F5-7553-4D6D-9F4D-04962062FDE7}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_MaxCurrentLimit_2</Property>
	<Property Name="varPersistentID:{1BB1822C-9132-4DAA-904D-1BD782D8F35B}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_TVTriggerPolarity</Property>
	<Property Name="varPersistentID:{1C133591-6A7F-45AE-8B8B-5C9ADF563D40}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_VoltageLevel_2</Property>
	<Property Name="varPersistentID:{1D489469-F1D8-4AD0-93CA-58B1D8A3335B}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Set-SoftwareTrigger</Property>
	<Property Name="varPersistentID:{1DB3E6B3-64CB-4D65-88D1-1D576E97B0A8}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_CircuitMode_2</Property>
	<Property Name="varPersistentID:{1DB46C09-8F75-4F66-9808-20D6B27B855D}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_DI</Property>
	<Property Name="varPersistentID:{1DEE6FDA-E1E3-418E-8009-251CC6DE48D8}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_VerticalRange_2</Property>
	<Property Name="varPersistentID:{1EA3E8ED-71DC-4EBA-A9A7-90A4B8BA7363}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_OVPLimit_1</Property>
	<Property Name="varPersistentID:{1EB750DE-20A8-49B1-8A19-F2A283B6CE48}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_AI5</Property>
	<Property Name="varPersistentID:{1F6CCB1C-CB46-45DD-A932-0059FBD39652}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Amplitude_0</Property>
	<Property Name="varPersistentID:{201C7477-60FA-45B2-84BC-0AB769CD3A5A}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{20AFEB73-FF72-4D1C-AE1B-30E340F970B2}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_FilterTime_3</Property>
	<Property Name="varPersistentID:{20DE5255-C209-4C8B-9FE0-AA63D3FB593C}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_VerticalRange_0</Property>
	<Property Name="varPersistentID:{216BE0F6-3368-45BF-8B30-EBC66ED67FC4}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Waveform_0</Property>
	<Property Name="varPersistentID:{21F3FF3A-7011-4106-A543-1F748617DAFA}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_CircuitMode_1</Property>
	<Property Name="varPersistentID:{2259000F-9AEA-4EE0-8111-31B4E5C29FC7}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{230A9320-579A-4AC9-BDC3-4405B08BA03F}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_AM-ModulationDepth</Property>
	<Property Name="varPersistentID:{233D2C35-E8CA-495D-B7E5-99833E9833C0}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_PollingCounter</Property>
	<Property Name="varPersistentID:{24482464-DC72-43AA-9B25-27C7D747278D}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_SPS_B</Property>
	<Property Name="varPersistentID:{249C440B-6E2A-4CB8-A4B0-1B2F4BBEDDDF}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_MaxInputFrequency_3</Property>
	<Property Name="varPersistentID:{24F4766A-85D8-4683-9491-D5D6C2020F39}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_TriggerType</Property>
	<Property Name="varPersistentID:{255597DE-83A3-4AAC-BBB8-413FE84B1FD0}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_LowReference</Property>
	<Property Name="varPersistentID:{262CC555-6C85-4777-A2AA-ADA2910C5B29}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_ResourceName</Property>
	<Property Name="varPersistentID:{266413BA-6060-4276-AE92-F8BF1A1F25F3}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_VerticalCoupling_0</Property>
	<Property Name="varPersistentID:{26AB211E-ED0A-4E1C-AA77-1C3F3EC62F19}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_FirmwareRevision</Property>
	<Property Name="varPersistentID:{27C601CD-3AF5-4963-A36E-C90DC8DAE1B7}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/myBaseActor_PollingCounter</Property>
	<Property Name="varPersistentID:{27D2BBFA-B354-49DA-9740-63947A587B4F}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_TriggerSlope</Property>
	<Property Name="varPersistentID:{29A666FB-61F1-4329-A2A5-7934748AA4D5}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/myDeviceActor_PollingInterval</Property>
	<Property Name="varPersistentID:{2A6E826A-63C8-4E0A-910F-052DD37E6682}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Set-ArbitrarySequence</Property>
	<Property Name="varPersistentID:{2B044091-678E-4BE4-99BB-BA93FFB08F1E}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_AM-Source_0</Property>
	<Property Name="varPersistentID:{2B7E74AB-BD6F-4F4C-801E-3262DF339094}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_TriggerVoltageLevel_1</Property>
	<Property Name="varPersistentID:{2BA24690-9447-49D8-A982-35C80098384D}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_Behavior_1</Property>
	<Property Name="varPersistentID:{2BF8149F-AB97-42D9-A58B-9A65F3AC9186}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_DI2</Property>
	<Property Name="varPersistentID:{2D0B7F3A-61B1-4BF2-9629-3CE2EE9AB261}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_AI1</Property>
	<Property Name="varPersistentID:{2D16B64D-9AB6-401A-A59B-F38A37330C47}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Set-OutputMode</Property>
	<Property Name="varPersistentID:{2E8613A5-BADF-4156-B27D-BAAE706A1A58}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Set-AMSource_0</Property>
	<Property Name="varPersistentID:{2F5F23DF-0151-4805-8FD8-E72B54207608}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_DO7</Property>
	<Property Name="varPersistentID:{30871832-3264-4142-93A1-BC4D4B8501A5}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_OVPEnabled_2</Property>
	<Property Name="varPersistentID:{309842BF-A60C-496B-9710-B4721CF2F7F7}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_PollingIterations</Property>
	<Property Name="varPersistentID:{30A7ECAB-B24F-48A7-ADF6-CBF655E04C22}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_Waveform</Property>
	<Property Name="varPersistentID:{3129F30F-49ED-47E8-A02D-55BDB63E4779}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_Set-DO_5</Property>
	<Property Name="varPersistentID:{313AEB0F-9507-48CB-BFB9-3CF16DC4D7B0}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_ProbeAttenuation_2</Property>
	<Property Name="varPersistentID:{315E1E4A-6C54-4FCD-AC82-D05471F6B928}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_Counter0</Property>
	<Property Name="varPersistentID:{320AF0CA-8DD4-423E-83EF-9DAA64D184F9}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_TriggerHoldoff</Property>
	<Property Name="varPersistentID:{3324B139-4B89-4E73-9950-027EEDE097F4}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_TVEvent</Property>
	<Property Name="varPersistentID:{339912F9-CD58-484B-9B79-5E27EB5DAC28}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_ErrorMessage</Property>
	<Property Name="varPersistentID:{3452649D-2A47-4E05-9071-76BDD9550469}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_MeasCompleteDestination</Property>
	<Property Name="varPersistentID:{3477CF32-8D8B-41D5-BC0A-F204D4C4A71D}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_ErrorMessage</Property>
	<Property Name="varPersistentID:{34BB882C-18E4-415D-97B4-5DAD812D1C06}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Set-SoftwareHoldTrigger</Property>
	<Property Name="varPersistentID:{354343F1-1871-4BE8-B906-F207158F5F3F}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_ApertureTimeUnit</Property>
	<Property Name="varPersistentID:{358BDB87-7007-4A48-BE4C-13E5E4DC760D}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Set-SampleClock</Property>
	<Property Name="varPersistentID:{360B7355-A1BA-47CD-AFA4-8598E91A84D2}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_MidReference</Property>
	<Property Name="varPersistentID:{3643FDC0-96AE-4453-9C45-F7D2806E979D}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_MaxVoltageLevel_2</Property>
	<Property Name="varPersistentID:{364DBE8B-59BB-46AD-8A87-6CA5548BB361}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_ErrorCode</Property>
	<Property Name="varPersistentID:{3660C124-8F9E-4094-B5A3-0AAB2F8B195A}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_PollingDeltaT</Property>
	<Property Name="varPersistentID:{3684861F-D955-40F7-AFAA-1ACF37302A1A}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_VerticalCoupling_2</Property>
	<Property Name="varPersistentID:{36B0CAFB-6E51-4936-A44F-CED5B2647271}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_PollingTime</Property>
	<Property Name="varPersistentID:{372F13D2-AF32-42A8-8D95-282C2178FC4F}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_PowerLineFrequency</Property>
	<Property Name="varPersistentID:{37D73F87-EB6C-4F44-AEFA-95153878267C}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/myBaseActor_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{38853DC6-B7D2-402C-AAA2-52FF3E516760}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_AM-Enabled_0</Property>
	<Property Name="varPersistentID:{38EEDFBF-F6A1-4658-BE37-FFFF7C666485}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_AI0</Property>
	<Property Name="varPersistentID:{3A0BF733-D54A-4EC0-9C1D-933B012AB5C1}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_VerticalOffset_0</Property>
	<Property Name="varPersistentID:{3A94FFA8-167F-470A-8DF5-23E6A26E22DC}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_TriggerCoupling</Property>
	<Property Name="varPersistentID:{3A9A2490-E672-48FB-B95D-BF687B0179ED}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_TriggerSource_2</Property>
	<Property Name="varPersistentID:{3C6A9A25-371A-4E7C-9E39-6C55F13672A5}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_AI4</Property>
	<Property Name="varPersistentID:{3E611B61-CE10-4E79-B2E6-BA04C591C64A}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_OVPLimit_2</Property>
	<Property Name="varPersistentID:{3E737045-29C5-418E-95D6-C2FCA1732B9C}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/myDeviceActor_PollingCounter</Property>
	<Property Name="varPersistentID:{3E9D5BBF-B8A4-4A00-9BBD-B7A623DAAA2F}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/myDeviceActor_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{3F97EFA5-0D2D-4098-AEB5-CA932F14CA78}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_SelftestResultCode</Property>
	<Property Name="varPersistentID:{3FD5F32F-9CF2-49DD-8829-4484F196718D}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Set-BurstCount_0</Property>
	<Property Name="varPersistentID:{405428F5-60A9-4FBC-A70D-4DF83957B746}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_PollingCounter</Property>
	<Property Name="varPersistentID:{41EE8045-9C61-4ED3-A2D9-EC4CEFFBA6FC}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{42398282-1FF2-4C8D-BED0-C86D93ED0782}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_AO0</Property>
	<Property Name="varPersistentID:{42C986B4-3891-4817-829A-AB1D7DF1D9C5}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_Set-SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{438F9BC2-0C01-44CA-B1AA-2F1D04CE8A86}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/ObjectManager_PollingInterval</Property>
	<Property Name="varPersistentID:{4410F14C-544A-47E6-B50B-F5388A03FFB1}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_InState_2</Property>
	<Property Name="varPersistentID:{4589334D-DCE2-4E54-96DC-355CF6918822}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_ReferenceJunctionType</Property>
	<Property Name="varPersistentID:{46310A99-9EA2-49E8-9567-61DF2E429AC2}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300-Proxy_Activate</Property>
	<Property Name="varPersistentID:{4A6114D7-D779-4F0E-B51B-08EE2E6BF4AC}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/myDeviceActor_ErrorCode</Property>
	<Property Name="varPersistentID:{4B40BD6D-A791-42EA-8411-E6C2298D3067}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_Set-FilterTime_0</Property>
	<Property Name="varPersistentID:{4B6A42C1-FFE3-4F24-AE16-8A5C57C8C395}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_SampleClockSource</Property>
	<Property Name="varPersistentID:{4BEE5E42-3877-4D5F-B994-C95510876946}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_PollingTime</Property>
	<Property Name="varPersistentID:{4C2C67A8-6EDB-4654-9536-52B27CBF732C}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_PollingMode</Property>
	<Property Name="varPersistentID:{4C7A01E3-091E-4E54-8ABF-F08BA15948EA}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_DriverRevision</Property>
	<Property Name="varPersistentID:{4D46CE68-53FE-446C-898C-2974850C8334}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_ChannelEnabled_2</Property>
	<Property Name="varPersistentID:{4DA8CEC6-155E-4031-BD4C-1448DB1B44B3}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_Set-SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{4DC2F052-A3A2-4BA7-AACA-B2BC4D39C8B0}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_AI6</Property>
	<Property Name="varPersistentID:{4E3CCF2A-99AC-47B0-8D1B-4C5048E2720E}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_DO</Property>
	<Property Name="varPersistentID:{4FA54EFC-256D-4BE3-9B12-DC7F0B10F527}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_TriggerSource_1</Property>
	<Property Name="varPersistentID:{4FE39FDD-0F8F-44CC-A8CF-573FEF49BD19}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Set-FMInternal</Property>
	<Property Name="varPersistentID:{5091A374-B5EF-4B78-A1A0-C212ADE2FB73}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Set-AdvancedTrigger_0</Property>
	<Property Name="varPersistentID:{50AB81B2-1DCE-4F3C-81C3-B0AB2A306FDE}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_Status_1</Property>
	<Property Name="varPersistentID:{5141AFA1-7453-4C99-A538-CD60B2913B3E}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_AI2</Property>
	<Property Name="varPersistentID:{517CCC40-7F15-42FC-B8FE-56DC3047DE6F}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_Pressure_3</Property>
	<Property Name="varPersistentID:{52BB56DE-3341-4665-9F58-09F8EA1E625E}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Set-InternalTriggerRate</Property>
	<Property Name="varPersistentID:{52DB26A6-2EF5-4C7C-8E8A-6D75D2385F58}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_RuntHighThreshold</Property>
	<Property Name="varPersistentID:{5305BA57-AAC4-4166-91B6-AFB5B8174300}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_AdvancedTriggerSource_0</Property>
	<Property Name="varPersistentID:{5319E935-CB97-44B9-8155-52D1E15CAE51}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_ResourceName</Property>
	<Property Name="varPersistentID:{5347BBD2-339C-48CD-A9CA-39D851574132}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_VerticalCoupling_1</Property>
	<Property Name="varPersistentID:{5455D123-1C94-4FDB-9463-D6BAED466DAE}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/myDouble</Property>
	<Property Name="varPersistentID:{55931D32-28B7-4559-BD31-24BFBAB6FD86}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_AI</Property>
	<Property Name="varPersistentID:{57F6EDCC-7E70-4A97-BC4D-45782443A6D1}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_PollingInterval</Property>
	<Property Name="varPersistentID:{5849A390-DAE3-46E1-A44B-C6383F5BCDC9}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_InState_0</Property>
	<Property Name="varPersistentID:{58BFD667-2FEA-4804-BB45-CD6E51907599}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_PollingMode</Property>
	<Property Name="varPersistentID:{58DB1287-CADB-4315-8CE9-5A382FDF4E1C}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_DCOffset_0</Property>
	<Property Name="varPersistentID:{596460E2-EC58-4636-B41C-78B9BDD55265}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_Status_3</Property>
	<Property Name="varPersistentID:{59DBB18E-77BE-4A47-B152-4A32D3A08277}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{5A3E28F8-95C7-468C-A7F5-527BC35394A0}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300-Proxy_WorkerActor</Property>
	<Property Name="varPersistentID:{5A856257-4D6C-4BB2-BDDC-4156027242B5}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_Measure_V_0</Property>
	<Property Name="varPersistentID:{5B27442E-4EEE-4BA9-837A-75782C4F6BA2}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_PollingTime</Property>
	<Property Name="varPersistentID:{5B3CFF49-D0B3-4B1A-8409-2A194FA14134}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_VerticalOffset_1</Property>
	<Property Name="varPersistentID:{5B8B8617-2F60-45D5-8441-C65C49421A66}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_Set-SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{5CB0340F-3C96-4C14-B94C-D9665BCD1D12}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_DI0</Property>
	<Property Name="varPersistentID:{5CEEA6B2-8420-41C9-B38B-48C8880BA94C}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_TriggerVoltageLevel_2</Property>
	<Property Name="varPersistentID:{5D4D41ED-070C-4EF5-937F-FBBD9349F0F0}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_DriverRevision</Property>
	<Property Name="varPersistentID:{5E24F809-6A90-49BD-B6B5-A00137BDBC0C}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Set-SampleRate</Property>
	<Property Name="varPersistentID:{5E495EB4-F771-4D50-BF50-222958006B69}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_InterpolationMethod</Property>
	<Property Name="varPersistentID:{5E618DE9-BABE-494F-81D7-E2C35142222C}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_MaxInputFrequency_1</Property>
	<Property Name="varPersistentID:{5EF8C348-5053-4425-B966-248130F125C1}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Set-OutputEnable_0</Property>
	<Property Name="varPersistentID:{5FB59005-4D59-4A8A-808F-55FA65EFE784}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_DI3</Property>
	<Property Name="varPersistentID:{6068CECB-9B32-4764-BF92-25BE051696BC}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/myDeviceActor_ErrorMessage</Property>
	<Property Name="varPersistentID:{61613AB8-70A8-4AC5-9F12-69389BC356E8}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_OutputEnable_2</Property>
	<Property Name="varPersistentID:{618A7078-B725-4EA3-8D86-F1958238896D}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_Set-AO_0</Property>
	<Property Name="varPersistentID:{61D5DCB4-8C01-469C-9EB0-D8E09917A6D5}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_ContinousAcquisition</Property>
	<Property Name="varPersistentID:{6377E306-6748-415E-B334-0CDEDA34BD26}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Set-SoftwareStopTrigger</Property>
	<Property Name="varPersistentID:{64145DB2-7997-4B9A-AEE6-FB5C0124AF39}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/myBaseActor_PollingMode</Property>
	<Property Name="varPersistentID:{64C15EB6-6205-4100-B9BA-1E651F49A1F8}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_DriverRevision</Property>
	<Property Name="varPersistentID:{657539CB-4DD8-4415-826E-80ACA6823CC4}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_Measure_A_2</Property>
	<Property Name="varPersistentID:{6681F352-33E7-4320-ADB9-C124079CA5E4}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_ErrorCode</Property>
	<Property Name="varPersistentID:{6794B383-D647-4B05-BA0A-6E19912A517B}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Set-FMSource_0</Property>
	<Property Name="varPersistentID:{67BE401B-6DD0-4A28-B28F-C2A995DA264E}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_ThermocoupleType</Property>
	<Property Name="varPersistentID:{683CF5F9-D924-4D01-AEB3-D82E8B386DEA}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_Set-DO_4</Property>
	<Property Name="varPersistentID:{6A04617D-CC55-421F-9095-79634C0CB2E7}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_AM-ModulationFrequency</Property>
	<Property Name="varPersistentID:{6BA68DB5-B52C-41C8-8A23-B16B5F691450}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_StartTriggerSource_0</Property>
	<Property Name="varPersistentID:{6BD94E1F-DA66-4E8C-9507-95873DCFED73}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_SetID</Property>
	<Property Name="varPersistentID:{6C1F8268-DD54-4CE0-ACB9-3E78FDFEA435}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_RangeType_2</Property>
	<Property Name="varPersistentID:{6D5CCFF2-A418-4497-ABA9-29C99ABDECBC}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_CircuitMode_0</Property>
	<Property Name="varPersistentID:{6F3FA23E-B30E-431E-9CBA-772AF0F3DD9B}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_Set-DO_0</Property>
	<Property Name="varPersistentID:{701215CA-6018-4D2A-9C54-A6AA88EFF759}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_Range_2</Property>
	<Property Name="varPersistentID:{710B5A79-4E0A-4429-8D90-CC8DB8CDD1A7}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_PollingCounter</Property>
	<Property Name="varPersistentID:{7110004A-B85A-4EF9-8CD5-4BDC2999D048}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_ChannelEnabled_1</Property>
	<Property Name="varPersistentID:{7321F93A-26D9-4FEA-8C68-BAE631E05233}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/myBaseActor_PollingTime</Property>
	<Property Name="varPersistentID:{73AEE683-AF10-49B8-B0DD-A25256326639}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_DesiredOutputState_1</Property>
	<Property Name="varPersistentID:{73ECA6B6-B43E-4019-9179-57CAEC4A4925}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_TriggerVoltageLevel_0</Property>
	<Property Name="varPersistentID:{7505A45D-E725-49FD-A29E-0A4343FD4469}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/myDeviceProxy_Activate</Property>
	<Property Name="varPersistentID:{762E2292-931D-4F23-BC19-0440A9ED6F9B}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_SPS_2</Property>
	<Property Name="varPersistentID:{7630FE1C-D5EA-4589-AC6E-2BD573C7293E}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/myBaseActor_PollingInterval</Property>
	<Property Name="varPersistentID:{76CDB795-FD56-4433-AA50-87E03BDD6D32}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_PollingInterval</Property>
	<Property Name="varPersistentID:{7720AF04-6E4B-4AA8-A2E7-1BBB4845F13F}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_RangeType_0</Property>
	<Property Name="varPersistentID:{7779C3E9-4C8C-445F-94FE-29827A7AE36D}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/myDeviceActor_PollingIterations</Property>
	<Property Name="varPersistentID:{7785BBE2-84E2-466C-8EA1-633BD4F5F891}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_SampleTrigger</Property>
	<Property Name="varPersistentID:{77901AA1-D358-4C8B-8EFD-46F90C2FADB2}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/myDeviceActor_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{77F74B64-7502-4F88-A504-863CFACAA5F2}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_ErrorCode</Property>
	<Property Name="varPersistentID:{78BFA22A-DE37-405F-B0D9-37A6F82AECAE}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{790E4906-4913-41F1-86E2-61A40FCFD7B4}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_DesiredOutputState_0</Property>
	<Property Name="varPersistentID:{79A5904B-3475-4CF6-9942-9295F934836A}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_PollingTime</Property>
	<Property Name="varPersistentID:{7BA23F9F-AF5C-487A-A63B-C6FE099E46BB}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_SelfTest</Property>
	<Property Name="varPersistentID:{7BD7884F-F320-4E51-92F1-5F4B40F76DD7}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_FirmwareRevision</Property>
	<Property Name="varPersistentID:{7C79AFE5-2A95-4F9B-A223-47768D58BA69}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Set-OutputImpedance_0</Property>
	<Property Name="varPersistentID:{7E350F17-BE61-4A3A-9A41-0EFD63614548}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_Pressure_0</Property>
	<Property Name="varPersistentID:{7E3C7955-6769-456C-A483-9A9A0B088C75}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/myDeviceProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{7E6EFE5C-D0BB-4127-9F58-BC1D00EDA453}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{7EE18A2A-1083-467E-A981-84FDF68A40A3}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_Set-DO</Property>
	<Property Name="varPersistentID:{801EAB50-FC29-455D-B4A4-3BA1494CA6C5}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{80763A02-9B28-4082-A996-38B67DEABA70}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{80D64E3A-8060-4696-8CF0-B8429ECF752D}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/myDeviceActor_PollingTime</Property>
	<Property Name="varPersistentID:{81B704B6-F8D1-4C05-8E58-CC63AF0E6984}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_CurrentLimit_1</Property>
	<Property Name="varPersistentID:{81DE8017-ED18-4D3A-B9BC-C55ECF6FA234}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_SPS_A</Property>
	<Property Name="varPersistentID:{8235D680-AB4C-4326-A507-0E4F7653080D}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_BurstCount_0</Property>
	<Property Name="varPersistentID:{8266B0C7-1447-45B8-8708-5622B93EE24B}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Set-FMEnabled_0</Property>
	<Property Name="varPersistentID:{83142F5E-FA37-44FD-8DDF-DA82401A6C7E}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_Set-CircuitMode_3</Property>
	<Property Name="varPersistentID:{84A0A475-9DB6-421B-B5A4-3088669DAA71}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{858687EE-BC19-4494-B9E7-103D368349E6}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_TriggerCount</Property>
	<Property Name="varPersistentID:{85C88D54-4C95-48B5-8DBD-FBE4743AE655}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_ProbeAttenuation_1</Property>
	<Property Name="varPersistentID:{86FBF947-9FC1-48EA-93D7-566F1A3A9527}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/myDeviceActor_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{87CDA31E-D80A-4F71-A21B-EA18AA623967}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_Set-CircuitMode_0</Property>
	<Property Name="varPersistentID:{87CFC688-1EC4-4E9E-BF32-475A580E8FF8}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_UnderrangeControl</Property>
	<Property Name="varPersistentID:{885FD895-6B89-4FAA-A124-26CB088A507B}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_OVPLimit_0</Property>
	<Property Name="varPersistentID:{88ABE69A-0ED1-4B33-942D-040A2A9F42DF}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_MaxCurrentLimit_0</Property>
	<Property Name="varPersistentID:{8919B009-16D7-47FC-9780-F67E57A5A06E}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_InState_1</Property>
	<Property Name="varPersistentID:{891C971F-A29D-48E0-87EB-B9ADE7937753}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_AI3</Property>
	<Property Name="varPersistentID:{8936FAB1-31D2-4ED2-8D85-913B5504651E}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_VoltageLevel_0</Property>
	<Property Name="varPersistentID:{89A7336B-B0B5-47E8-A9AC-B60B82A487AD}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_Set-CircuitMode_1</Property>
	<Property Name="varPersistentID:{89EF95B1-3E59-4E59-A974-71CBDED13DEF}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_TriggerCurrentLimit_1</Property>
	<Property Name="varPersistentID:{89F433C0-1D99-44AF-B906-C6406EB28F0C}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_CurrentLimit_0</Property>
	<Property Name="varPersistentID:{8A4ED9EA-3293-4D84-81B4-75D132705F44}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_Set-CircuitMode_2</Property>
	<Property Name="varPersistentID:{8AEFE4D3-59D9-4E46-B3DC-1F978D5D7B4E}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_OutputImpedance_0</Property>
	<Property Name="varPersistentID:{8B38F485-3614-43FB-B6BB-F90E61A2020D}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_Resistance</Property>
	<Property Name="varPersistentID:{8B8C83A8-864C-4492-AC54-06814415048C}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_Measure_A_1</Property>
	<Property Name="varPersistentID:{8BB9B0A6-D10C-4F18-A99D-BDE9672688CB}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_VoltageLevel_1</Property>
	<Property Name="varPersistentID:{8C5380E4-3DC8-4B31-9D64-2FE639CA0992}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_SelfTest</Property>
	<Property Name="varPersistentID:{8C65D44D-D5CE-4470-AECD-F7999CA8E3BB}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_MaxCurrentLimit_1</Property>
	<Property Name="varPersistentID:{8CC6F114-1C28-4B7C-BDC9-6914C7FFB420}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_AcquisitionStartTime</Property>
	<Property Name="varPersistentID:{8CD64584-2D36-4721-9997-EEE49B2CCFA8}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_Alpha</Property>
	<Property Name="varPersistentID:{8D0E34B9-7689-409A-93B9-3AB86790BE04}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_MaxInputFrequency_2</Property>
	<Property Name="varPersistentID:{8D6106DA-AC9A-498C-86E4-29354294AA28}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_ErrorMessage</Property>
	<Property Name="varPersistentID:{8DF5505A-81F4-4B46-B4EF-3CC2F187A817}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_DI6</Property>
	<Property Name="varPersistentID:{8F30B34F-01B7-4A27-9998-90AA058820A1}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_ErrorCode</Property>
	<Property Name="varPersistentID:{8FE6A247-69DE-4884-B924-B744D1C12B8B}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_Set-SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{90611E03-8642-4D9F-9779-BBE858B385DF}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_SampleInterval</Property>
	<Property Name="varPersistentID:{9121F6AA-8927-4CFB-BB59-53449170DF96}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Set-StandardWaveform_</Property>
	<Property Name="varPersistentID:{91CCD3E6-77EA-4C53-A900-7A65B0B8F303}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_Set-SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{91F1FE5C-44AB-4D5C-8BFA-67B0A7A03BDF}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_CurrentLimit_2</Property>
	<Property Name="varPersistentID:{925EA7A9-C76F-4989-A28B-6F2F976DD6A6}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/myBaseProxy_Activate</Property>
	<Property Name="varPersistentID:{927FBD32-9E70-4441-B652-FC1B867D74AF}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_DI1</Property>
	<Property Name="varPersistentID:{92E15DF0-0636-4955-BBB5-E9BC4FD271FF}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_ErrorCode</Property>
	<Property Name="varPersistentID:{93188AD1-CBC5-4F89-8E15-159A3FD56764}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_TVLineNumber</Property>
	<Property Name="varPersistentID:{9364396D-569A-4BF7-A08D-1827F8D9749B}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_Set-DO_7</Property>
	<Property Name="varPersistentID:{937097C7-3B53-4699-A54D-5B204AF46EBA}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_PollingIterations</Property>
	<Property Name="varPersistentID:{93981D33-6188-489C-8F65-5B85FC001620}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_GlitchWidth</Property>
	<Property Name="varPersistentID:{93EF3538-4220-484E-8B60-6E13B4F77382}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_WidthLowThreshold</Property>
	<Property Name="varPersistentID:{940CE3F3-451C-4F21-9B17-E4D6C294F58D}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_Set-DO_3</Property>
	<Property Name="varPersistentID:{94499DC5-E069-4967-87AF-67656C51AE1F}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_Behavior_2</Property>
	<Property Name="varPersistentID:{95111BDD-003B-4714-A705-CA451A3E035D}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_Set-FilterTime_3</Property>
	<Property Name="varPersistentID:{95FDA0D1-F67C-46FE-BF86-A5D6AC8A006F}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/myDeviceActor_ResourceName</Property>
	<Property Name="varPersistentID:{96F071C6-EC15-4D01-9FE5-B4A52636C079}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_Set-UnderrangeControl</Property>
	<Property Name="varPersistentID:{97A8CBFA-9960-4699-B63B-C3268A67A0DC}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_DO2</Property>
	<Property Name="varPersistentID:{99472E1E-DCD7-4D57-B99E-4B90EB7DC8B6}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/ObjectManager_PollingIterations</Property>
	<Property Name="varPersistentID:{9976B8DA-CCBD-4223-952A-B83F51013378}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_IsOverRange</Property>
	<Property Name="varPersistentID:{99925C2A-B6A1-46B3-A06E-1A77457D6193}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_Pressure_1</Property>
	<Property Name="varPersistentID:{99950165-AAB3-4423-B111-7EC88449AE26}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_PollingInterval</Property>
	<Property Name="varPersistentID:{99DF5CF6-0F97-4A5B-975E-81145C4DC293}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_ResourceName</Property>
	<Property Name="varPersistentID:{9BB94546-31A0-4BC3-8A0A-7F6BD81E9233}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Set-TriggerSource_0</Property>
	<Property Name="varPersistentID:{9BCD0D29-D1CF-4E34-9511-DD0A6D05D279}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_FilterTime_2</Property>
	<Property Name="varPersistentID:{9C5EC432-39A4-4E31-BC7A-420667597806}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_DI4</Property>
	<Property Name="varPersistentID:{9DA10BE2-BC23-4C43-91E9-8C88172F3F86}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQProxy_Activate</Property>
	<Property Name="varPersistentID:{9DB9CCDA-331E-4F00-B0B9-CC7E22FEAB34}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_VerticalRange_3</Property>
	<Property Name="varPersistentID:{9E2D8145-A500-425D-95FF-103A25DA30AD}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_ACMaxFrequency</Property>
	<Property Name="varPersistentID:{9EC91743-251F-4CD3-BBFD-69D8AE267CCC}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Frequency_0</Property>
	<Property Name="varPersistentID:{9EE09F0E-5324-4A00-A141-2BD9C7C4E357}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_AdvancedTriggerSlope_0</Property>
	<Property Name="varPersistentID:{9F53982E-1C92-414A-970C-97060BD1CE1A}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_FilterTime_0</Property>
	<Property Name="varPersistentID:{9F6FC508-9E9C-4556-95C7-765B2565B757}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_SampleRate</Property>
	<Property Name="varPersistentID:{9F747458-D29B-418A-B4EB-2123A83B34D5}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_HighReference</Property>
	<Property Name="varPersistentID:{A00D6C7C-83D9-4555-9664-AAE4DF7DAA2B}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_RangeType_1</Property>
	<Property Name="varPersistentID:{A0B6A660-063F-4D37-A3DB-E8654BD283A2}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_FirmwareRevision</Property>
	<Property Name="varPersistentID:{A0DA5BC1-36B2-4005-8FB7-83597F4C010E}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_TriggerSource</Property>
	<Property Name="varPersistentID:{A14D82DE-2A24-4113-B71A-9CE973CD442A}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/myDeviceActor_SelfTest</Property>
	<Property Name="varPersistentID:{A1767E6B-7A62-4180-BF8F-815890CB8EE3}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_Activate</Property>
	<Property Name="varPersistentID:{A1CFBE99-C2DD-45D4-9775-EB9C5B3E5053}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{A1EE7F1A-1D32-46FC-A42A-3FB11C968656}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Set-StartTrigger_0</Property>
	<Property Name="varPersistentID:{A2F610E6-1DB7-4A90-AD5E-B3949F313D31}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{A304443B-2906-453A-8A05-B6DD3D95F008}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_AO</Property>
	<Property Name="varPersistentID:{A3051F49-2E6D-4002-90F7-AA86B8186D64}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_ChannelEnabled_3</Property>
	<Property Name="varPersistentID:{A53DCD9B-22D4-4E96-B095-54953D18DE65}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_Set-DOi</Property>
	<Property Name="varPersistentID:{A5C960C2-0C18-4A00-8D0D-E7B985C93221}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/myDeviceActor_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{A60E582F-D338-46A1-B8B0-E1972BB31DBF}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_ActualSampleMode</Property>
	<Property Name="varPersistentID:{A79EEA14-8846-4FD2-83F5-C3914E053508}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Set-Generation</Property>
	<Property Name="varPersistentID:{A86C425D-D7AB-428C-80F3-FA0D6755FCB0}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_FM-ModulationWaveform</Property>
	<Property Name="varPersistentID:{A88ABF21-5A18-459C-AF7D-C34D13460C5B}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_PollingInterval</Property>
	<Property Name="varPersistentID:{A8F074C9-80B4-4978-AC60-C299E5A86AB9}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_PollingDeltaT</Property>
	<Property Name="varPersistentID:{A964C0EB-DD8E-497F-AFD2-BB6F50DF94F2}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_DO1</Property>
	<Property Name="varPersistentID:{AAD24E2F-1ED8-40BF-82AD-D436111E2399}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_Set-DO_6</Property>
	<Property Name="varPersistentID:{AB131BA1-87FB-4D51-B883-AA456ACC3F7B}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Set-AMInternal</Property>
	<Property Name="varPersistentID:{AB1CEB15-675A-4C7C-A8B2-185139536483}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Set-AMEnabled_0</Property>
	<Property Name="varPersistentID:{AB6E4F8B-F6EE-490F-B6E6-737219D90FC8}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_Status_2</Property>
	<Property Name="varPersistentID:{AC1ED1EB-DBC4-4D90-9421-D1764861FF74}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_DI7</Property>
	<Property Name="varPersistentID:{ACA1D0C6-5D1C-40D9-988A-E78BD91E3B65}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_ResumeTriggerSlope_0</Property>
	<Property Name="varPersistentID:{AD3FA98E-F055-4D04-9F23-2168E1B1BB2B}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_ApertureTime</Property>
	<Property Name="varPersistentID:{AE301417-71BE-4748-84DF-807CB6AD26A9}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/myDeviceActor_FirmwareRevision</Property>
	<Property Name="varPersistentID:{AEFD7DD6-96CF-4E3A-829F-6CEABE7E079A}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_PollingDeltaT</Property>
	<Property Name="varPersistentID:{AF155A7B-A31D-4C98-B7F8-32B4900D4E23}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_ErrorMessage</Property>
	<Property Name="varPersistentID:{B0594538-F26D-4057-AC94-EA9DF58A627C}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_FrequencyVoltageRange</Property>
	<Property Name="varPersistentID:{B0A1B7B9-30FB-4C19-ACB3-E7E15EEF8F6C}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_PollingCounter</Property>
	<Property Name="varPersistentID:{B0C77559-A2FC-4894-A3DE-A43113BDAE3A}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/ObjectManager_PollingDeltaT</Property>
	<Property Name="varPersistentID:{B1119A16-492D-42A3-9348-C9057EA9D35A}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_Reading</Property>
	<Property Name="varPersistentID:{B1841482-8443-4FE4-B4A2-9560CDD0A8F5}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_ChannelEnabled_0</Property>
	<Property Name="varPersistentID:{B21F1AD7-7BC0-4AC3-9127-AA6CA0820B57}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_Measure_V_1</Property>
	<Property Name="varPersistentID:{B38796CF-302F-4058-9C58-93AEDB436996}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_DI5</Property>
	<Property Name="varPersistentID:{B4742F97-5414-4D48-928F-65C5B70355E1}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/myBaseActor_PollingIterations</Property>
	<Property Name="varPersistentID:{B487FDC7-A198-4392-8254-598CCEAF1193}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_AbsoluteResolution</Property>
	<Property Name="varPersistentID:{B4B4A3D1-738B-47AB-A4DC-4F54828B53A3}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/myDeviceActor_SelftestResultCode</Property>
	<Property Name="varPersistentID:{B5EEFE3F-BF68-4BA9-869F-B9875F7E1B66}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_Pressure_2</Property>
	<Property Name="varPersistentID:{B65279AD-4C12-4ABD-AB18-DF1D8700DB84}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_ResumeTriggerSource_0</Property>
	<Property Name="varPersistentID:{B66F9D4D-2F53-4D10-8D2E-A6C9A9431B0C}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_RuntLowThreshold</Property>
	<Property Name="varPersistentID:{B766B023-6DC1-403A-AF14-8215BB725179}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_AO1</Property>
	<Property Name="varPersistentID:{B7739168-3DBE-4396-980A-7EACD3E7F4BA}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{B7C51843-0298-490C-8A09-54D3CBFC645C}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_PollingCounter</Property>
	<Property Name="varPersistentID:{B870F166-71A2-4672-B581-3D9A64ABF4BD}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_FilterTime_1</Property>
	<Property Name="varPersistentID:{B937BD55-D539-4EB8-8A2E-5A7EF3948578}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/myDeviceActor_PollingMode</Property>
	<Property Name="varPersistentID:{BA90A36F-7375-4CEA-B257-841F2474A294}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_AM-ModulationWaveform</Property>
	<Property Name="varPersistentID:{BA97537F-1B8E-4581-AB43-AFF50434FDE0}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_SampleClockOutputEnabled</Property>
	<Property Name="varPersistentID:{BA9AAF55-6289-4DAE-BCAE-7982E1D9B287}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_StartTriggerSlope_0</Property>
	<Property Name="varPersistentID:{BB060B80-3856-491E-A456-DB155152739F}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_InputImpedance_3</Property>
	<Property Name="varPersistentID:{BB5FA543-1191-467F-BEF5-4893489379CE}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_Measure_A_0</Property>
	<Property Name="varPersistentID:{BBE3D6FA-AD36-4762-A925-AC7C7A2EB9D4}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_SampleCount</Property>
	<Property Name="varPersistentID:{BC4D3B62-B7AB-4913-84E0-22438C220C97}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_FM-Source_0</Property>
	<Property Name="varPersistentID:{BD6BAE75-C2F2-4018-86F2-BD66F3E74848}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_TriggerRate</Property>
	<Property Name="varPersistentID:{BE36759A-62C5-4CA1-9F70-30E6FC79F238}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Arb-Frequency_0</Property>
	<Property Name="varPersistentID:{BEAD4236-446B-42BC-9F98-63EE86613E04}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_ResourceName</Property>
	<Property Name="varPersistentID:{BFCB36B1-BD52-470A-AA83-8398E27581A7}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_ProbeAttenuation_3</Property>
	<Property Name="varPersistentID:{BFDBECD1-7537-46CA-A322-7982FDFDA85C}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_Set-DO_1</Property>
	<Property Name="varPersistentID:{BFFE9020-76EA-4DAD-A259-730FDE607BD2}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_OutputMode</Property>
	<Property Name="varPersistentID:{C0154620-98DF-4539-8770-6D15CAB2CE00}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_FirmwareRevision</Property>
	<Property Name="varPersistentID:{C13FB00A-3C35-4FAC-91B1-99C136C7DA04}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_WidthHighThreshold</Property>
	<Property Name="varPersistentID:{C1A6FF3A-3840-4F9C-A42F-604106DA4F2E}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_FirmwareRevision</Property>
	<Property Name="varPersistentID:{C1F0D97A-8C79-4953-9DD8-46422BE128C6}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_AcquisitionType</Property>
	<Property Name="varPersistentID:{C2C7B3EE-2C3F-42CB-8BC9-FF31DBC81EB3}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_InputImpedance_2</Property>
	<Property Name="varPersistentID:{C440A173-1A2A-4A79-8E31-109E7F25E1F6}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_Set-SetID</Property>
	<Property Name="varPersistentID:{C74D2786-8103-4DDD-BEEB-5B6DA7B39313}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_InputImpedance_1</Property>
	<Property Name="varPersistentID:{C7969955-5605-40C0-91AD-02E163C38735}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_TriggerLevel</Property>
	<Property Name="varPersistentID:{C875E2FD-B55D-4DD5-AC8F-5A205BBF2AE0}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_ActualSampleRate</Property>
	<Property Name="varPersistentID:{C9134B6B-8D88-4F82-8B20-F7A168247811}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_PollingStartStop</Property>
	<Property Name="varPersistentID:{C93F7ADD-5D2E-46FB-A262-41E3014077D1}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{C983FF62-D057-4B16-9BA9-921C9798BD21}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_TriggerModifier</Property>
	<Property Name="varPersistentID:{CA0B0216-9B8D-4A07-AEBB-B18F8634D209}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_Behavior_0</Property>
	<Property Name="varPersistentID:{CA3E8372-6D4E-4EBD-9E55-1019948E1F34}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_ACMinFrequency</Property>
	<Property Name="varPersistentID:{CA9EDF60-5E1C-44B2-BD17-4F6699C9DC50}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_DesiredOutputState_2</Property>
	<Property Name="varPersistentID:{CBE00A87-59A7-4C8B-B6B2-788719BAD42E}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/ObjectManager_PollingTime</Property>
	<Property Name="varPersistentID:{CC123D9A-60F8-4727-A502-EF5A79214F4A}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_SelftestResultCode</Property>
	<Property Name="varPersistentID:{CC45A66F-19CA-4157-8AF5-DAAD67B7F8B4}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_OperationMode_0</Property>
	<Property Name="varPersistentID:{CC5B3A00-6CAF-4FB8-90AC-C788705E724E}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/ObjectManager_PollingCounter</Property>
	<Property Name="varPersistentID:{CDA91F76-0D79-4C47-8B32-6A0BBEA116C9}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_SPS</Property>
	<Property Name="varPersistentID:{CE6147BF-2787-44DB-B189-D97BEA5CDC81}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/myDeviceActor_Reset</Property>
	<Property Name="varPersistentID:{CEC2B225-3645-45BF-8BA2-7CDDFB6CD945}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_AI7</Property>
	<Property Name="varPersistentID:{CEE79E30-29F6-467F-BD9B-CFF5687AEDAB}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_Reset</Property>
	<Property Name="varPersistentID:{CEF0A04D-C20F-4FA2-A689-FFE0FC35598F}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Set-ArbitraryWaveform</Property>
	<Property Name="varPersistentID:{CF43C766-D030-4D2B-A97B-D5EAE9E591C6}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_MaxVoltageLevel_1</Property>
	<Property Name="varPersistentID:{CFA30C58-3267-4C28-A318-ACF9A8A3A1BE}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_TriggerDelay</Property>
	<Property Name="varPersistentID:{D054791A-8B87-43F3-95AF-8CDA05D35585}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_OutputEnable_1</Property>
	<Property Name="varPersistentID:{D09F3708-F0F6-4B06-9907-ED9A8B64A048}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_PollingTime</Property>
	<Property Name="varPersistentID:{D21F45AC-61CF-4EBC-AC0B-BFCA0507C8FD}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_DriverRevision</Property>
	<Property Name="varPersistentID:{D25F98F0-EF04-4931-A1BC-D213636E1A5C}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_TriggerCurrentLimit_0</Property>
	<Property Name="varPersistentID:{D2631A3F-65F3-4079-A519-965AC062D7C5}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_DriverRevision</Property>
	<Property Name="varPersistentID:{D2B98901-1FCF-4F25-B414-922C60EFCD9E}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_Set-SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{D30832EE-50BA-4803-AA9A-EFAE5D78171F}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_PollingMode</Property>
	<Property Name="varPersistentID:{D36C3B64-83A6-4519-B70E-5621C38E4FC7}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_Set-DO_2</Property>
	<Property Name="varPersistentID:{D42126AC-8CD6-495B-BFBE-E4FD9023B163}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_Set-AO</Property>
	<Property Name="varPersistentID:{D4439EB5-5E14-4ED1-9A32-F65B1CE2BA90}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/myDeviceActor_DriverRevision</Property>
	<Property Name="varPersistentID:{D52ED424-56B0-4B4A-B0BD-38991045C1C8}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_TriggerSource</Property>
	<Property Name="varPersistentID:{D6C946CA-5943-4D0F-AE01-14E572F74070}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_OVPEnabled_0</Property>
	<Property Name="varPersistentID:{D6DFF082-4BC7-4943-A323-276FD456110E}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_AcquisitionStatus</Property>
	<Property Name="varPersistentID:{D7B8E04D-9392-4832-9043-C0EFE36D4177}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_SelftestResultCode</Property>
	<Property Name="varPersistentID:{D8ABBF68-965D-4507-9054-0DC2D16C3A1B}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_PollingDeltaT</Property>
	<Property Name="varPersistentID:{D8DD5224-2E3A-48A1-94C0-86B3B388B2AE}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_SelftestResultCode</Property>
	<Property Name="varPersistentID:{D933967A-4780-45A1-8D25-FEB26149DE86}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/myBaseActor_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{DA1CDDFA-53D2-4D34-A0F0-80075A6A762B}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_TriggerSlope</Property>
	<Property Name="varPersistentID:{DAD6DDDD-6BF1-4DCC-9FC0-2925776C98C4}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_WidthCondition</Property>
	<Property Name="varPersistentID:{DBC33E50-98FE-48B4-AB3F-4F0776196E15}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_SPS_1</Property>
	<Property Name="varPersistentID:{DC03C144-D953-4271-BCCA-406A98C9C777}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_ErrorMessage</Property>
	<Property Name="varPersistentID:{DCB852BF-FD1A-4BC1-97D2-86DD67B42571}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_Range</Property>
	<Property Name="varPersistentID:{DD141DAF-BDCF-475D-AFC2-72E9B2B98624}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_DO0</Property>
	<Property Name="varPersistentID:{DDC102AD-71D2-4B53-86F5-A1FCD98FBEDE}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_ActualRange</Property>
	<Property Name="varPersistentID:{DDD7DC6A-F92A-4D92-BEF7-683FF1A8525E}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_ProbeAttenuation_0</Property>
	<Property Name="varPersistentID:{DEAB79EB-1ACB-421A-983A-D721F2ED6367}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_ErrorCode</Property>
	<Property Name="varPersistentID:{DEBD9F87-CF77-4747-BC26-AF5BB0A9FE29}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/myBaseActor_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{DEFECDD6-23BA-4D75-B7B5-75CAF01E45E2}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_SPS_3</Property>
	<Property Name="varPersistentID:{DF448881-C3D7-4103-98BB-A77F46D5713F}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_Set-FilterTime_1</Property>
	<Property Name="varPersistentID:{DFE12718-47CA-40A4-9C50-BB1071973770}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_VerticalRange_1</Property>
	<Property Name="varPersistentID:{E16697E3-48C5-44EA-8667-BEDF02818E84}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Set-ResumeTrigger_0</Property>
	<Property Name="varPersistentID:{E24674DD-4CDB-43D2-959C-2D01B7F8B5FC}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_DO6</Property>
	<Property Name="varPersistentID:{E2D61AC3-AEED-4D58-8F64-E1F5C893B2A8}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_Set-FilterTime_2</Property>
	<Property Name="varPersistentID:{E38D45D5-D80D-4C2C-99EF-3842CDEBB2EF}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_DO5</Property>
	<Property Name="varPersistentID:{E473C06D-AE55-4CBE-88CC-AC04AA3BA636}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_DO4</Property>
	<Property Name="varPersistentID:{E4873119-6EDE-41DA-A189-0EB2F0EFC00B}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_NumberOfAverages</Property>
	<Property Name="varPersistentID:{E56214C1-2536-4048-BB27-846518C87F27}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_ResourceName</Property>
	<Property Name="varPersistentID:{E5A28B15-CFAE-48BC-9638-C88BEEAB4336}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_FM-ModulationFrequency</Property>
	<Property Name="varPersistentID:{E5BAC8CA-E892-4528-8E8F-921AB9BABE91}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_ActualRecordLength</Property>
	<Property Name="varPersistentID:{E5DD707A-8249-4A1A-9B66-571B0A3129FD}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_GlitchCondition</Property>
	<Property Name="varPersistentID:{E6644478-3F13-47A6-B6EF-9D83972667C1}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_SelftestResultCode</Property>
	<Property Name="varPersistentID:{E72D99BC-2224-4D7A-8EFE-2EAB1A12A818}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_GlitchPolarity</Property>
	<Property Name="varPersistentID:{E74534F0-EE99-4A81-BF3A-1F409D23C187}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_TriggerSource_0</Property>
	<Property Name="varPersistentID:{E7DAAEFE-A899-4EF2-8AB7-D40DF99FDFB6}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_VerticalOffset_2</Property>
	<Property Name="varPersistentID:{E809E8F5-A259-46AA-BB60-EC3C4FA2592F}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_InputImpedance_0</Property>
	<Property Name="varPersistentID:{E8520FE2-0D53-49C8-8A9E-36F201FFCB24}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{E8EEC6B1-11B3-48F6-AB13-5861CE3E1556}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{E9C58797-B66F-4D21-9CE3-B41A41E738AD}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_Range_0</Property>
	<Property Name="varPersistentID:{EAF40391-7EBE-4946-87FB-5323BAEA85C8}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_ErrorMessage</Property>
	<Property Name="varPersistentID:{EC0EDA57-776A-4D58-918E-36D1632DEBA1}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Set-RefClockSource</Property>
	<Property Name="varPersistentID:{EC2035AE-BDFD-4F37-BE83-1ACF44A2B1BE}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_SelftestResultCode</Property>
	<Property Name="varPersistentID:{ECCBA639-DA2C-4513-81EA-84BFD6D2329A}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/ObjectManager_PollingMode</Property>
	<Property Name="varPersistentID:{EE589C48-A4FC-4FB8-BE15-64C3211C1E07}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_PollingDeltaT</Property>
	<Property Name="varPersistentID:{EEB03CAA-F5D1-42A1-9EA1-523D0B344B36}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_TransducerType</Property>
	<Property Name="varPersistentID:{EF52B584-FD77-4B47-A357-6CE11C81F817}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_Reset</Property>
	<Property Name="varPersistentID:{EF662C23-1C29-42F0-87F9-DA71E1983C71}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_TVSignalFormat</Property>
	<Property Name="varPersistentID:{EFB7C645-02C2-4681-9B4F-102E8D8D2706}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_DO3</Property>
	<Property Name="varPersistentID:{F01E62A3-F711-4A3C-A31F-8B48937B83C3}" Type="Ref">/My Computer/Packages/BNT/myDAQ.lvlib/myDAQ_Counter</Property>
	<Property Name="varPersistentID:{F30E2D96-31F4-4202-A78C-AB8182DEFA72}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_ActualTimeIncrement</Property>
	<Property Name="varPersistentID:{F3B70151-B392-460F-9B00-CAB680EE0B60}" Type="Ref">/My Computer/Packages/TPG300/TPG300-SV.lvlib/TPG300_FirmwareRevision</Property>
	<Property Name="varPersistentID:{F4E92EC3-7223-4F79-AA61-929F78079218}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_MaxInputFrequency_0</Property>
	<Property Name="varPersistentID:{F59E93D1-8686-42C6-9D4A-62492517BC58}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_OVPEnabled_1</Property>
	<Property Name="varPersistentID:{F5B56CDA-BEF9-49B4-814D-26AB329BBE6D}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_OutputEnable_0</Property>
	<Property Name="varPersistentID:{F6EE4464-4942-4862-BDCC-AB12BCA44D17}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_StartPhase_0</Property>
	<Property Name="varPersistentID:{F6F47DEA-4908-47D7-8962-46DDC0364E33}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_PollingMode</Property>
	<Property Name="varPersistentID:{F7CEB762-5926-47F6-B7F1-80F654D66FCF}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_ChannelEnabled_0</Property>
	<Property Name="varPersistentID:{F83407D4-6834-49E2-ACFC-DC3663F82606}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_NumberOfEnvelopes</Property>
	<Property Name="varPersistentID:{F8F6547D-5E1E-4BAD-8ABD-5F74617D9DF4}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_FM-PeakDeviation</Property>
	<Property Name="varPersistentID:{F9B5B1FD-AABB-4447-9CA4-F632F386F9B5}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDMM_MeasurementFunction</Property>
	<Property Name="varPersistentID:{FAEFB9E4-656B-41DE-8DB1-3000F99BDDD7}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_PollingIterations</Property>
	<Property Name="varPersistentID:{FB42E655-2E79-4213-AAEB-97FEBD2667F0}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_Range_1</Property>
	<Property Name="varPersistentID:{FBC6F49A-44BC-4563-A50E-57EFF99CFC35}" Type="Ref">/My Computer/Packages/CSPP_Core/CSPP_Core_SV.lvlib/myBaseActor_PollingDeltaT</Property>
	<Property Name="varPersistentID:{FCCBDF64-30D3-43E9-A0C8-A29619A33EBD}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_WidthPolarity</Property>
	<Property Name="varPersistentID:{FCD491BA-D22D-4802-9B85-CF0AD232152A}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_StopTriggerSlope_0</Property>
	<Property Name="varPersistentID:{FD2DC8FA-00EE-4F32-ADEA-E07A765BB54D}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myScope_VerticalOffset_3</Property>
	<Property Name="varPersistentID:{FD8F35DE-92FF-4D54-A579-0D10EE1AD45D}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_ReferenceClockSource</Property>
	<Property Name="varPersistentID:{FE365626-35F7-4AF6-8E7B-23F286EDF1AD}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myDCPwr_NumberOfChannels</Property>
	<Property Name="varPersistentID:{FEC209BA-F4F2-44BB-BC85-A96EE9EC6095}" Type="Ref">/My Computer/Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib/myFgen_Set-SampleClockOutputEnabled</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="AF.lib" Type="Folder">
			<Item Name="Actor Framework.lvlib" Type="Library" URL="/&lt;vilib&gt;/ActorFramework/Actor Framework.lvlib"/>
			<Item Name="AF Debug.lvlib" Type="Library" URL="/&lt;resource&gt;/AFDebug/AF Debug.lvlib"/>
			<Item Name="Batch Msg.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/ActorFramework/Batch Msg/Batch Msg.lvclass"/>
			<Item Name="Linked Network Actor.lvlib" Type="Library" URL="/&lt;vilib&gt;/NI/Actors/Linked Network Actor/Linked Network Actor.lvlib"/>
		</Item>
		<Item Name="Docs" Type="Folder">
			<Property Name="NI.SortType" Type="Int">0</Property>
			<Item Name="Actor Framework.pptx" Type="Document" URL="../Docs/Actor Framework.pptx"/>
			<Item Name="CS++ &amp; DSC AE Monitoring@CSWS2017.pptx" Type="Document" URL="../Docs/CS++ &amp; DSC AE Monitoring@CSWS2017.pptx"/>
			<Item Name="CS++@CSWS2015.pptx" Type="Document" URL="../Docs/CS++@CSWS2015.pptx"/>
			<Item Name="CS++@VIP2013.pptx" Type="Document" URL="../Docs/CS++@VIP2013.pptx"/>
			<Item Name="LVDevDay2015 - fertig.pptx" Type="Document" URL="../Docs/LVDevDay2015 - fertig.pptx"/>
			<Item Name="README.md" Type="Document" URL="../README.md"/>
			<Item Name="VIP_2016_CSPP.pptx" Type="Document" URL="../Docs/VIP_2016_CSPP.pptx"/>
			<Item Name="VIP_2017_GSI Presentation.pptx" Type="Document" URL="../Docs/VIP_2017_GSI Presentation.pptx"/>
		</Item>
		<Item Name="EUPL License" Type="Folder">
			<Item Name="EUPL v.1.1 - Lizenz.pdf" Type="Document" URL="../Packages/CSPP_Core/EUPL v.1.1 - Lizenz.pdf"/>
			<Item Name="EUPL v.1.1 - Lizenz.rtf" Type="Document" URL="../Packages/CSPP_Core/EUPL v.1.1 - Lizenz.rtf"/>
		</Item>
		<Item Name="instr.lib" Type="Folder">
			<Item Name="InstrSim.lvlib" Type="Library" URL="../instr.lib/InstrSim/InstrSim.lvlib"/>
			<Item Name="Leybold 23xxx2.lvlib" Type="Library" URL="/&lt;instrlib&gt;/Leybold23xxx2/Leybold 23xxx2.lvlib"/>
			<Item Name="TPG300.lvlib" Type="Library" URL="../instr.lib/TPG300/TPG300.lvlib"/>
		</Item>
		<Item Name="Packages" Type="Folder">
			<Property Name="NI.SortType" Type="Int">0</Property>
			<Item Name="BNT" Type="Folder">
				<Item Name="BNT_DAQmx.lvlib" Type="Library" URL="../Packages/BNT/BNT_DAQmx/BNT_DAQmx.lvlib"/>
				<Item Name="BNT_DSCXtGraph.lvlib" Type="Library" URL="../Packages/BNT/BNT_DSCXtGraph/BNT_DSCXtGraph.lvlib"/>
				<Item Name="BNT_DSCXYGraph.lvlib" Type="Library" URL="../Packages/BNT/BNT_DSCXYGraph/BNT_DSCXYGraph.lvlib"/>
				<Item Name="myDAQ.lvlib" Type="Library" URL="../Packages/BNT/BNT_DAQmx/myDAQ.lvlib"/>
			</Item>
			<Item Name="CS_Workshop" Type="Folder">
				<Item Name="CS-Workshop.ini" Type="Document" URL="../Packages/CS_Workshop/CS-Workshop.ini"/>
				<Item Name="CS_WorkshopContent.vi" Type="VI" URL="../Packages/CS_Workshop/CS_WorkshopContent.vi"/>
				<Item Name="CSPP_InstrSim.lvlib" Type="Library" URL="../Packages/CS_Workshop/Actors/CSPP_InstrSim/CSPP_InstrSim.lvlib"/>
				<Item Name="CSPP_InstrSimCtrl.lvlib" Type="Library" URL="../Packages/CS_Workshop/Actors/CSPP_InstrSimCtrl/CSPP_InstrSimCtrl.lvlib"/>
				<Item Name="CSPP_InstrSimGUI.lvlib" Type="Library" URL="../Packages/CS_Workshop/Actors/CSPP_InstrSimGUI/CSPP_InstrSimGUI.lvlib"/>
			</Item>
			<Item Name="CSPP_Core" Type="Folder">
				<Item Name="Actors" Type="Folder">
					<Property Name="NI.SortType" Type="Int">0</Property>
					<Item Name="CSPP_BaseActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_BaseActor/CSPP_BaseActor.lvlib"/>
					<Item Name="CSPP_DeviceActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DeviceActor/CSPP_DeviceActor.lvlib"/>
					<Item Name="CSPP_DeviceGUIActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DeviceGUIActor/CSPP_DeviceGUIActor.lvlib"/>
					<Item Name="CSPP_DSMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DSMonitor/CSPP_DSMonitor.lvlib"/>
					<Item Name="CSPP_GUIActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_GUIActor/CSPP_GUIActor.lvlib"/>
					<Item Name="CSPP_PVMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_PVMonitor/CSPP_PVMonitor.lvlib"/>
					<Item Name="CSPP_PVProxy.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_PVProxy/CSPP_PVProxy.lvlib"/>
					<Item Name="CSPP_StartActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_StartActor/CSPP_StartActor.lvlib"/>
					<Item Name="CSPP_SVMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_SVMonitor/CSPP_SVMonitor.lvlib"/>
					<Item Name="CSPP_TDMSStorage.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_TDMSStorage/CSPP_TDMSStorage.lvlib"/>
				</Item>
				<Item Name="Classes" Type="Folder">
					<Property Name="NI.SortType" Type="Int">0</Property>
					<Item Name="CSPP_BaseClasses.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_BaseClasses/CSPP_BaseClasses.lvlib"/>
					<Item Name="CSPP_ProcessVariables.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_ProcessVariables/CSPP_ProcessVariables.lvlib"/>
					<Item Name="CSPP_SharedVariables.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_ProcessVariables/SVConnection/CSPP_SharedVariables.lvlib"/>
				</Item>
				<Item Name="Documentation" Type="Folder">
					<Item Name="Change_Log.txt" Type="Document" URL="../Packages/CSPP_Core/Change_Log.txt"/>
					<Item Name="CSPP_Core-errors.txt" Type="Document" URL="../Packages/CSPP_Core/CSPP_Core-errors.txt"/>
					<Item Name="CSPP_Core.ini" Type="Document" URL="../Packages/CSPP_Core/CSPP_Core.ini"/>
					<Item Name="README.md" Type="Document" URL="../Packages/CSPP_Core/README.md"/>
					<Item Name="Release_Notes.txt" Type="Document" URL="../Packages/CSPP_Core/Release_Notes.txt"/>
				</Item>
				<Item Name="Libraries" Type="Folder">
					<Property Name="NI.SortType" Type="Int">0</Property>
					<Item Name="CSPP_Base.lvlib" Type="Library" URL="../Packages/CSPP_Core/Libraries/Base/CSPP_Base.lvlib"/>
					<Item Name="CSPP_Utilities.lvlib" Type="Library" URL="../Packages/CSPP_Core/Libraries/Utilities/CSPP_Utilities.lvlib"/>
				</Item>
				<Item Name="Messages" Type="Folder">
					<Property Name="NI.SortType" Type="Int">0</Property>
					<Item Name="CSPP_AEUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_AEUpdate Msg/CSPP_AEUpdate Msg.lvlib"/>
					<Item Name="CSPP_AsyncCallbackMsg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_AsyncCallbackMsg/CSPP_AsyncCallbackMsg.lvlib"/>
					<Item Name="CSPP_DataUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_DataUpdate Msg/CSPP_DataUpdate Msg.lvlib"/>
					<Item Name="CSPP_PVUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_PVUpdate Msg/CSPP_PVUpdate Msg.lvlib"/>
				</Item>
				<Item Name="CSPP_Core_SV.lvlib" Type="Library" URL="../Packages/CSPP_Core/CSPP_Core_SV.lvlib"/>
				<Item Name="CSPP_CoreContent.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_CoreContent.vi"/>
				<Item Name="CSPP_CoreGUIContent.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_CoreGUIContent.vi"/>
			</Item>
			<Item Name="CSPP_DeviceBase" Type="Folder">
				<Property Name="NI.SortType" Type="Int">0</Property>
				<Item Name="Documentation" Type="Folder">
					<Item Name="CSPP_DeviceBase-errors.txt" Type="Document" URL="../Packages/CSPP_DeviceBase/CSPP_DeviceBase-errors.txt"/>
					<Item Name="CSPP_DeviceBase.ini" Type="Document" URL="../Packages/CSPP_DeviceBase/CSPP_DeviceBase.ini"/>
					<Item Name="README.md" Type="Document" URL="../Packages/CSPP_DeviceBase/README.md"/>
				</Item>
				<Item Name="CSPP_DCPwr.lvlib" Type="Library" URL="../Packages/CSPP_DeviceBase/CSPP_DCPwr/CSPP_DCPwr.lvlib"/>
				<Item Name="CSPP_DCPwrGui.lvlib" Type="Library" URL="../Packages/CSPP_DeviceBase/CSPP_DCPwrGui/CSPP_DCPwrGui.lvlib"/>
				<Item Name="CSPP_DeviceBase-Content.vi" Type="VI" URL="../Packages/CSPP_DeviceBase/CSPP_DeviceBase-Content.vi"/>
				<Item Name="CSPP_DeviceBaseSV.lvlib" Type="Library" URL="../Packages/CSPP_DeviceBase/CSPP_DeviceBaseSV.lvlib"/>
				<Item Name="CSPP_DMM.lvlib" Type="Library" URL="../Packages/CSPP_DeviceBase/CSPP_DMM/CSPP_DMM.lvlib"/>
				<Item Name="CSPP_DMMGui.lvlib" Type="Library" URL="../Packages/CSPP_DeviceBase/CSPP_DMMGui/CSPP_DMMGui.lvlib"/>
				<Item Name="CSPP_Fgen.lvlib" Type="Library" URL="../Packages/CSPP_DeviceBase/CSPP_Fgen/CSPP_Fgen.lvlib"/>
				<Item Name="CSPP_FgenGui.lvlib" Type="Library" URL="../Packages/CSPP_DeviceBase/CSPP_FgenGui/CSPP_FgenGui.lvlib"/>
				<Item Name="CSPP_MCS.lvlib" Type="Library" URL="../Packages/CSPP_DeviceBase/CSPP_MCS/CSPP_MCS.lvlib"/>
				<Item Name="CSPP_Motor.lvlib" Type="Library" URL="../Packages/CSPP_DeviceBase/CSPP_Motor/CSPP_Motor.lvlib"/>
				<Item Name="CSPP_MotorGui.lvlib" Type="Library" URL="../Packages/CSPP_DeviceBase/CSPP_MotorGui/CSPP_MotorGui.lvlib"/>
				<Item Name="CSPP_PPG.lvlib" Type="Library" URL="../Packages/CSPP_DeviceBase/CSPP_PPG/CSPP_PPG.lvlib"/>
				<Item Name="CSPP_PPGGui.lvlib" Type="Library" URL="../Packages/CSPP_DeviceBase/CSPP_PPGGui/CSPP_PPGGui.lvlib"/>
				<Item Name="CSPP_Scope.lvlib" Type="Library" URL="../Packages/CSPP_DeviceBase/CSPP_Scope/CSPP_Scope.lvlib"/>
				<Item Name="CSPP_ScopeGui.lvlib" Type="Library" URL="../Packages/CSPP_DeviceBase/CSPP_ScopeGui/CSPP_ScopeGui.lvlib"/>
			</Item>
			<Item Name="CSPP_DIM" Type="Folder">
				<Item Name="Documentation" Type="Folder">
					<Item Name="Change_Log.txt" Type="Document" URL="../Packages/CSPP_DIM/Change_Log.txt"/>
					<Item Name="DIM.ini" Type="Document" URL="../Packages/CSPP_DIM/DIM.ini"/>
					<Item Name="README.txt" Type="Document" URL="../Packages/CSPP_DIM/README.txt"/>
					<Item Name="Release_Notes.txt" Type="Document" URL="../Packages/CSPP_DIM/Release_Notes.txt"/>
				</Item>
				<Item Name="CSPP_DIM.lvlib" Type="Library" URL="../Packages/CSPP_DIM/CSPP_DIM.lvlib"/>
				<Item Name="CSPP_DIMContent.vi" Type="VI" URL="../Packages/CSPP_DIM/CSPP_DIMContent.vi"/>
				<Item Name="CSPP_DIMMonitor.lvlib" Type="Library" URL="../Packages/CSPP_DIM/DIM_Monitor/CSPP_DIMMonitor.lvlib"/>
				<Item Name="DimIndicators.lvlib" Type="Library" URL="../Packages/DimLVEvent/DimIndicators/DimIndicators.lvlib"/>
				<Item Name="LVDimInterface.lvlib" Type="Library" URL="../Packages/DimLVEvent/LVDimInterface/LVDimInterface.lvlib"/>
			</Item>
			<Item Name="CSPP_DSC" Type="Folder">
				<Item Name="Documentation" Type="Folder">
					<Item Name="Change_Log.txt" Type="Document" URL="../Packages/CSPP_DSC/Change_Log.txt"/>
					<Item Name="CSPP_DSC.ini" Type="Document" URL="../Packages/CSPP_DSC/CSPP_DSC.ini"/>
					<Item Name="myHTV.xml" Type="Document" URL="../Packages/CSPP_DSC/myHTV.xml"/>
					<Item Name="README.md" Type="Document" URL="../Packages/CSPP_DSC/README.md"/>
				</Item>
				<Item Name="CSPP_DSCAlarmViewer.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCAlarmViewer/CSPP_DSCAlarmViewer.lvlib"/>
				<Item Name="CSPP_DSCConnection.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Classes/DSCConnection/CSPP_DSCConnection.lvlib"/>
				<Item Name="CSPP_DSCContent.vi" Type="VI" URL="../Packages/CSPP_DSC/CSPP_DSCContent.vi"/>
				<Item Name="CSPP_DSCManager.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCManager/CSPP_DSCManager.lvlib"/>
				<Item Name="CSPP_DSCMonitor.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCMonitor/CSPP_DSCMonitor.lvlib"/>
				<Item Name="CSPP_DSCMsgLogger.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Classes/CSPP_DSCMsgLogger/CSPP_DSCMsgLogger.lvlib"/>
				<Item Name="CSPP_DSCTrendViewer.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCTrendViewer/CSPP_DSCTrendViewer.lvlib"/>
				<Item Name="DSC Remote SV Access.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Contributed/DSC Remote SV Access.lvlib"/>
				<Item Name="Test DSCConnection.vi" Type="VI" URL="../Packages/CSPP_DSC/Test DSCConnection.vi"/>
			</Item>
			<Item Name="CSPP_IVI" Type="Folder">
				<Item Name="Documentation" Type="Folder">
					<Item Name="Change_Log.txt" Type="Document" URL="../Packages/CSPP_IVI/Change_Log.txt"/>
					<Item Name="MAX_IVIconfigData.nce" Type="Document" URL="../Packages/CSPP_IVI/MAX_IVIconfigData.nce"/>
					<Item Name="README.txt" Type="Document" URL="../Packages/CSPP_IVI/README.txt"/>
					<Item Name="Release_Notes.txt" Type="Document" URL="../Packages/CSPP_IVI/Release_Notes.txt"/>
				</Item>
				<Item Name="IVI-Content.vi" Type="VI" URL="../Packages/CSPP_IVI/IVI-Content.vi"/>
				<Item Name="IVI_ACPwr.lvlib" Type="Library" URL="../Packages/CSPP_IVI/IVI_ACPwr/IVI_ACPwr.lvlib"/>
				<Item Name="IVI_Counter.lvlib" Type="Library" URL="../Packages/CSPP_IVI/IVI_Counter/IVI_Counter.lvlib"/>
				<Item Name="IVI_DCPwr.lvlib" Type="Library" URL="../Packages/CSPP_IVI/IVI_DCPwr/IVI_DCPwr.lvlib"/>
				<Item Name="IVI_DMM.lvlib" Type="Library" URL="../Packages/CSPP_IVI/IVI_DMM/IVI_DMM.lvlib"/>
				<Item Name="IVI_Fgen.lvlib" Type="Library" URL="../Packages/CSPP_IVI/IVI_Fgen/IVI_Fgen.lvlib"/>
				<Item Name="IVI_PwrMeter.lvlib" Type="Library" URL="../Packages/CSPP_IVI/IVI_PwrMeter/IVI_PwrMeter.lvlib"/>
				<Item Name="IVI_Scope.lvlib" Type="Library" URL="../Packages/CSPP_IVI/IVI_Scope/IVI_Scope.lvlib"/>
			</Item>
			<Item Name="CSPP_LNA" Type="Folder">
				<Item Name="Documentation" Type="Folder">
					<Item Name="CSPP_LNA-errors.txt" Type="Document" URL="../Packages/CSPP_LNA/CSPP_LNA-errors.txt"/>
					<Item Name="CSPP_LNAMessage.docx" Type="Document" URL="../Packages/CSPP_LNA/CSPP_LNAMessage.docx"/>
					<Item Name="CSPP_LNAMessage.pdf" Type="Document" URL="../Packages/CSPP_LNA/CSPP_LNAMessage.pdf"/>
					<Item Name="README.md" Type="Document" URL="../Packages/CSPP_LNA/README.md"/>
					<Item Name="TestLNA.ini" Type="Document" URL="../Packages/CSPP_LNA/TestLNA.ini"/>
				</Item>
				<Item Name="CSPP_LNA.lvlib" Type="Library" URL="../Packages/CSPP_LNA/CSPP_LNA.lvlib"/>
				<Item Name="CSPP_LNAContent.vi" Type="VI" URL="../Packages/CSPP_LNA/CSPP_LNAContent.vi"/>
				<Item Name="TestLNA.lvlib" Type="Library" URL="../Packages/CSPP_LNA/TestLNA/Test LNA/TestLNA.lvlib"/>
			</Item>
			<Item Name="CSPP_ObjectManager" Type="Folder">
				<Property Name="NI.SortType" Type="Int">0</Property>
				<Item Name="Documentation" Type="Folder">
					<Item Name="Change_Log.txt" Type="Document" URL="../Packages/CSPP_ObjectManager/Change_Log.txt"/>
					<Item Name="CSPP_ObjectManager.ini" Type="Document" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager.ini"/>
					<Item Name="README.txt" Type="Document" URL="../Packages/CSPP_ObjectManager/README.txt"/>
					<Item Name="Release_Notes.txt" Type="Document" URL="../Packages/CSPP_ObjectManager/Release_Notes.txt"/>
				</Item>
				<Item Name="CSPP_ObjectManager.lvlib" Type="Library" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager.lvlib"/>
				<Item Name="CSPP_ObjectManager_Content.vi" Type="VI" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager_Content.vi"/>
			</Item>
			<Item Name="CSPP_PVConverter" Type="Folder">
				<Item Name="CSPP_PV2ArrayConverter.lvlib" Type="Library" URL="../Packages/CSPP_PVConverter/CSPP_PV2ArrayConverter.lvlib"/>
				<Item Name="Monitor PVArray.vi" Type="VI" URL="../Packages/CSPP_PVConverter/CSPP_PVToArrayConverter/Monitor PVArray.vi"/>
			</Item>
			<Item Name="CSPP_Syslog" Type="Folder">
				<Item Name="Documentation" Type="Folder">
					<Item Name="CSPP_Syslog.ini" Type="Document" URL="../Packages/CSPP_Syslog/CSPP_Syslog.ini"/>
					<Item Name="README.md" Type="Document" URL="../Packages/CSPP_Syslog/README.md"/>
				</Item>
				<Item Name="CSPP_Syslog.lvlib" Type="Library" URL="../Packages/CSPP_Syslog/CSPP_Syslog.lvlib"/>
			</Item>
			<Item Name="CSPP_Utilities" Type="Folder">
				<Item Name="Documentation" Type="Folder">
					<Item Name="CSPP_Utilities.ini" Type="Document" URL="../Packages/CSPP_Utilities/CSPP_Utilities.ini"/>
					<Item Name="README.md" Type="Document" URL="../Packages/CSPP_Utilities/README.md"/>
				</Item>
				<Item Name="CSPP_BeepActor.lvlib" Type="Library" URL="../Packages/CSPP_Utilities/Actors/CSPP_BeepActor/CSPP_BeepActor.lvlib"/>
				<Item Name="CSPP_UtilitiesContent.vi" Type="VI" URL="../Packages/CSPP_Utilities/CSPP_UtilitiesContent.vi"/>
			</Item>
			<Item Name="Leybold" Type="Folder">
				<Item Name="Leybold23xxx2.lvlib" Type="Library" URL="../Packages/CSPP_Leybold/Leybold23xxx2.lvlib"/>
			</Item>
			<Item Name="TPG300" Type="Folder">
				<Item Name="TPG300-SV.lvlib" Type="Library" URL="../Packages/CSPP_TPG300/TPG300-SV.lvlib"/>
				<Item Name="TPG300A.lvlib" Type="Library" URL="../Packages/CSPP_TPG300/TPG300A.lvlib"/>
				<Item Name="TPG300GUI.lvlib" Type="Library" URL="../Packages/CSPP_TPG300/TPG300 GUI/TPG300GUI.lvlib"/>
			</Item>
		</Item>
		<Item Name="User" Type="Folder">
			<Item Name="CSPP_HBContent.vi" Type="VI" URL="../CSPP_HBContent.vi"/>
			<Item Name="Generate Proxy-Msg-URLs.vi" Type="VI" URL="../Test/Generate Proxy-Msg-URLs.vi"/>
			<Item Name="Syslog Collector Example.vi" Type="VI" URL="/&lt;vilib&gt;/NI/syslog/examples/Syslog Collector Example.vi"/>
			<Item Name="Test-P-R.vi" Type="VI" URL="../Test/Test-P-R.vi"/>
		</Item>
		<Item Name="CS++.ico" Type="Document" URL="../Docs/CS++.ico"/>
		<Item Name="CSPP_Dev.ini" Type="Document" URL="../CSPP_Dev.ini"/>
		<Item Name="CSPP_Main.vi" Type="VI" URL="../CSPP_Main.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Acquire Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Acquire Semaphore.vi"/>
				<Item Name="AddNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/AddNamedSemaphorePrefix.vi"/>
				<Item Name="ALM_Clear_UD_Alarm.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Clear_UD_Alarm.vi"/>
				<Item Name="ALM_Error_Resolve.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Error_Resolve.vi"/>
				<Item Name="ALM_Get_Alarms.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Get_Alarms.vi"/>
				<Item Name="ALM_Get_User_Name.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Get_User_Name.vi"/>
				<Item Name="ALM_GetTagURLs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_GetTagURLs.vi"/>
				<Item Name="ALM_Set_UD_Alarm.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Set_UD_Alarm.vi"/>
				<Item Name="ALM_Set_UD_Event.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Set_UD_Event.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Beep.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/Beep.vi"/>
				<Item Name="BuildErrorSource.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/BuildErrorSource.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Check Whether Timeouted.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/tagapi/internal/Check Whether Timeouted.vi"/>
				<Item Name="CIT_ReadTimeout.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/citadel/CIT_ReadTimeout.vi"/>
				<Item Name="citadel_ConvertDatabasePathToName.vi" Type="VI" URL="/&lt;vilib&gt;/citadel/citadel_ConvertDatabasePathToName.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Close Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Close Registry Key.vi"/>
				<Item Name="compatCalcOffset.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatCalcOffset.vi"/>
				<Item Name="compatFileDialog.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatFileDialog.vi"/>
				<Item Name="compatOpenFileOperation.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOpenFileOperation.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="CreateOrAddLibraryToParent.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/Variable/CreateOrAddLibraryToParent.vi"/>
				<Item Name="CreateOrAddLibraryToProject.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/Variable/CreateOrAddLibraryToProject.vi"/>
				<Item Name="CTL_dbNameValid.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_dbNameValid.vi"/>
				<Item Name="CTL_dbURLdecode.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_dbURLdecode.vi"/>
				<Item Name="CTL_defaultEvtDB.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_defaultEvtDB.vi"/>
				<Item Name="CTL_defaultHistDB.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_defaultHistDB.vi"/>
				<Item Name="CTL_defaultProcessName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_defaultProcessName.vi"/>
				<Item Name="CTL_extractURLMDPformat.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_extractURLMDPformat.vi"/>
				<Item Name="CTL_findDSCApp.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_findDSCApp.vi"/>
				<Item Name="CTL_getAllDBInfo.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_getAllDBInfo.vi"/>
				<Item Name="CTL_getArrayPathAndTraceReentrant.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_getArrayPathAndTraceReentrant.vi"/>
				<Item Name="CTL_getDBFromDir.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_getDBFromDir.vi"/>
				<Item Name="CTL_getDBPathandTraceList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_getDBPathandTraceList.vi"/>
				<Item Name="CTL_hdManager.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_hdManager.vi"/>
				<Item Name="CTL_hdManagerBuffer.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_hdManagerBuffer.vi"/>
				<Item Name="CTL_hdProxyManager.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_hdProxyManager.vi"/>
				<Item Name="CTL_lookupTagURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_lookupTagURL.vi"/>
				<Item Name="CTL_resolveSourceDBURLInput.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_resolveSourceDBURLInput.vi"/>
				<Item Name="DAQmx Clear Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Clear Task.vi"/>
				<Item Name="DAQmx Create Channel (AI-Acceleration-4 Wire DC Voltage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Acceleration-4 Wire DC Voltage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Acceleration-Accelerometer).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Acceleration-Accelerometer).vi"/>
				<Item Name="DAQmx Create Channel (AI-Acceleration-Charge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Acceleration-Charge).vi"/>
				<Item Name="DAQmx Create Channel (AI-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (AI-Charge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Charge).vi"/>
				<Item Name="DAQmx Create Channel (AI-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AI-Current-RMS).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Current-RMS).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Frequency-Voltage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Frequency-Voltage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-EddyCurrentProxProbe).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-EddyCurrentProxProbe).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-LVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-LVDT).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-RVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-RVDT).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Resistance).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Resistance).vi"/>
				<Item Name="DAQmx Create Channel (AI-Sound Pressure-Microphone).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Sound Pressure-Microphone).vi"/>
				<Item Name="DAQmx Create Channel (AI-Strain-Rosette Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Strain-Rosette Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Strain-Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Strain-Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Built-in Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Built-in Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-RTD).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-RTD).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermistor-Iex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermistor-Iex).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermistor-Vex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermistor-Vex).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermocouple).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermocouple).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Velocity-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Velocity-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-Custom with Excitation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-Custom with Excitation).vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-RMS).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-RMS).vi"/>
				<Item Name="DAQmx Create Channel (AO-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AO-FuncGen).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-FuncGen).vi"/>
				<Item Name="DAQmx Create Channel (AO-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (CI-Count Edges).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Count Edges).vi"/>
				<Item Name="DAQmx Create Channel (CI-Duty Cycle).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Duty Cycle).vi"/>
				<Item Name="DAQmx Create Channel (CI-Frequency).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Frequency).vi"/>
				<Item Name="DAQmx Create Channel (CI-GPS Timestamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-GPS Timestamp).vi"/>
				<Item Name="DAQmx Create Channel (CI-Period).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Period).vi"/>
				<Item Name="DAQmx Create Channel (CI-Position-Angular Encoder).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Position-Angular Encoder).vi"/>
				<Item Name="DAQmx Create Channel (CI-Position-Linear Encoder).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Position-Linear Encoder).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Freq).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Freq).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Ticks).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Ticks).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Time).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Time).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Width).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Width).vi"/>
				<Item Name="DAQmx Create Channel (CI-Semi Period).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Semi Period).vi"/>
				<Item Name="DAQmx Create Channel (CI-Two Edge Separation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Two Edge Separation).vi"/>
				<Item Name="DAQmx Create Channel (CI-Velocity-Angular).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Velocity-Angular).vi"/>
				<Item Name="DAQmx Create Channel (CI-Velocity-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Velocity-Linear).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Frequency).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Frequency).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Ticks).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Ticks).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Time).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Time).vi"/>
				<Item Name="DAQmx Create Channel (DI-Digital Input).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (DI-Digital Input).vi"/>
				<Item Name="DAQmx Create Channel (DO-Digital Output).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (DO-Digital Output).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Acceleration-Accelerometer).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Acceleration-Accelerometer).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Force-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Force-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Force-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Force-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Position-LVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Position-LVDT).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Position-RVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Position-RVDT).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Pressure-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Pressure-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Resistance).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Resistance).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Sound Pressure-Microphone).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Sound Pressure-Microphone).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Strain-Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Strain-Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-RTD).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-RTD).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Iex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Iex).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Vex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Vex).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermocouple).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermocouple).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Torque-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Torque-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Voltage-Custom with Excitation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Voltage-Custom with Excitation).vi"/>
				<Item Name="DAQmx Create Virtual Channel.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Virtual Channel.vi"/>
				<Item Name="DAQmx Fill In Error Info.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/miscellaneous.llb/DAQmx Fill In Error Info.vi"/>
				<Item Name="DAQmx Read (Analog 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D I16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D I16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D I32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D I32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Freq 1 Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Freq 1 Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Ticks 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Ticks 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Time 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Time 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Freq 1 Chan 1 Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Freq 1 Chan 1 Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Ticks 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Ticks 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Time 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Time 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Bool 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Bool 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Bool NChan 1Samp 1Line).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Bool NChan 1Samp 1Line).vi"/>
				<Item Name="DAQmx Read (Digital 1D U8 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U8 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U8 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U8 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U16 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U16 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U16 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U16 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 2D Bool NChan 1Samp NLine).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D Bool NChan 1Samp NLine).vi"/>
				<Item Name="DAQmx Read (Digital 2D U8 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U8 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital Bool 1Line 1Point).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Bool 1Line 1Point).vi"/>
				<Item Name="DAQmx Read (Digital U8 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U8 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital U16 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U16 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Raw 1D I8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I8).vi"/>
				<Item Name="DAQmx Read (Raw 1D I16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I16).vi"/>
				<Item Name="DAQmx Read (Raw 1D I32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I32).vi"/>
				<Item Name="DAQmx Read (Raw 1D U8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U8).vi"/>
				<Item Name="DAQmx Read (Raw 1D U16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U16).vi"/>
				<Item Name="DAQmx Read (Raw 1D U32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U32).vi"/>
				<Item Name="DAQmx Read.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read.vi"/>
				<Item Name="DAQmx Start Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Start Task.vi"/>
				<Item Name="DAQmx Stop Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Stop Task.vi"/>
				<Item Name="DAQmx Write (Analog 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D I16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D I16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D I32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D I32 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Frequency 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Frequency 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Frequency NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Frequency NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Ticks 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Ticks 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Time 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Time 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Time NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Time NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter 1DTicks NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1DTicks NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Frequency 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Frequency 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Ticks 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Ticks 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Time 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Time 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Bool 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Bool 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Bool NChan 1Samp 1Line).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Bool NChan 1Samp 1Line).vi"/>
				<Item Name="DAQmx Write (Digital 1D U8 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U8 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U8 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U8 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U16 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U16 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U16 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U16 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D Bool NChan 1Samp NLine).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D Bool NChan 1Samp NLine).vi"/>
				<Item Name="DAQmx Write (Digital 2D U8 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U8 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital Bool 1Line 1Point).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Bool 1Line 1Point).vi"/>
				<Item Name="DAQmx Write (Digital U8 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U8 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital U16 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U16 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Raw 1D I8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I8).vi"/>
				<Item Name="DAQmx Write (Raw 1D I16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I16).vi"/>
				<Item Name="DAQmx Write (Raw 1D I32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I32).vi"/>
				<Item Name="DAQmx Write (Raw 1D U8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U8).vi"/>
				<Item Name="DAQmx Write (Raw 1D U16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U16).vi"/>
				<Item Name="DAQmx Write (Raw 1D U32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U32).vi"/>
				<Item Name="DAQmx Write.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="Dflt Data Dir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Dflt Data Dir.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="dsc_PrefsPath.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/info/dsc_PrefsPath.vi"/>
				<Item Name="dscCommn.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/common/dscCommn.dll"/>
				<Item Name="dscHistD.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/historical/internal/dscHistD.dll"/>
				<Item Name="dscProc.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/process/dscProc.dll"/>
				<Item Name="DTbl Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital Size.vi"/>
				<Item Name="DTbl Uncompress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Uncompress Digital.vi"/>
				<Item Name="DWDT Uncompress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Uncompress Digital.vi"/>
				<Item Name="ERR_ErrorClusterFromErrorCode.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/error/ERR_ErrorClusterFromErrorCode.vi"/>
				<Item Name="ERR_GetErrText.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/error/ERR_GetErrText.vi"/>
				<Item Name="ERR_MergeErrors.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/error/ERR_MergeErrors.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="FileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInfo.vi"/>
				<Item Name="FileVersionInformation.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInformation.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="FindCloseTagByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindCloseTagByName.vi"/>
				<Item Name="FindElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElement.vi"/>
				<Item Name="FindElementStartByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElementStartByName.vi"/>
				<Item Name="FindEmptyElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindEmptyElement.vi"/>
				<Item Name="FindFirstTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindFirstTag.vi"/>
				<Item Name="FindMatchingCloseTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindMatchingCloseTag.vi"/>
				<Item Name="FixedFileInfo_Struct.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FixedFileInfo_Struct.ctl"/>
				<Item Name="Flush And Wait Empty Condition.ctl" Type="VI" URL="/&lt;vilib&gt;/dex/Flush And Wait Empty Condition.ctl"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get LV Class Default Value By Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value By Name.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="Get Project Library Version.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/Get Project Library Version.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetFileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfo.vi"/>
				<Item Name="GetFileVersionInfoSize.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfoSize.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/GetNamedSemaphorePrefix.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="HIST_AlarmDataToControl.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_AlarmDataToControl.vi"/>
				<Item Name="HIST_BuildAlarmColumns.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_BuildAlarmColumns.vi"/>
				<Item Name="HIST_CheckAlarmCtlRef.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_CheckAlarmCtlRef.vi"/>
				<Item Name="HIST_CheckTimestamps.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_CheckTimestamps.vi"/>
				<Item Name="HIST_DoDecimation.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_DoDecimation.vi"/>
				<Item Name="HIST_ExtractAlarmData.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_ExtractAlarmData.vi"/>
				<Item Name="HIST_FormatTagname&amp;ProcessFilterSpec.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_FormatTagname&amp;ProcessFilterSpec.vi"/>
				<Item Name="HIST_GET_FILTER_ERRORS.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_GET_FILTER_ERRORS.vi"/>
				<Item Name="HIST_GetFilterTime.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_GetFilterTime.vi"/>
				<Item Name="HIST_GetHistTagListCORE.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_GetHistTagListCORE.vi"/>
				<Item Name="HIST_ReadBitArrayTrace.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadBitArrayTrace.vi"/>
				<Item Name="HIST_ReadBitArrayTraceCORE.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadBitArrayTraceCORE.vi"/>
				<Item Name="HIST_ReadBitArrayTraces.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadBitArrayTraces.vi"/>
				<Item Name="HIST_ReadLogicalTrace.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadLogicalTrace.vi"/>
				<Item Name="HIST_ReadLogicalTraceCORE.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadLogicalTraceCORE.vi"/>
				<Item Name="HIST_ReadLogicalTraces.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadLogicalTraces.vi"/>
				<Item Name="HIST_ReadNumericTrace.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadNumericTrace.vi"/>
				<Item Name="HIST_ReadNumericTraceCORE.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadNumericTraceCORE.vi"/>
				<Item Name="HIST_ReadNumericTraces.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadNumericTraces.vi"/>
				<Item Name="HIST_ReadStringTrace.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadStringTrace.vi"/>
				<Item Name="HIST_ReadStringTraceCORE.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadStringTraceCORE.vi"/>
				<Item Name="HIST_ReadStringTraces.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadStringTraces.vi"/>
				<Item Name="HIST_ReadVariantTrace.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadVariantTrace.vi"/>
				<Item Name="HIST_ReadVariantTraceCORE.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadVariantTraceCORE.vi"/>
				<Item Name="HIST_ReadVariantTraces.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadVariantTraces.vi"/>
				<Item Name="HIST_RunAlarmQueryCORE.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_RunAlarmQueryCORE.vi"/>
				<Item Name="HIST_VALIDATE_FILTER.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_VALIDATE_FILTER.vi"/>
				<Item Name="HIST_ValReadTrendOptions.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ValReadTrendOptions.vi"/>
				<Item Name="IviACPwr Close.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviacpwr/_IviACPwr.llb/IviACPwr Close.vi"/>
				<Item Name="IviACPwr Error-Query.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviacpwr/_IviACPwr.llb/IviACPwr Error-Query.vi"/>
				<Item Name="IviACPwr Initialize With Options.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviacpwr/_IviACPwr.llb/IviACPwr Initialize With Options.vi"/>
				<Item Name="IviACPwr Initialize.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviacpwr/_IviACPwr.llb/IviACPwr Initialize.vi"/>
				<Item Name="IviACPwr IVI Error Converter.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviacpwr/_IviACPwr.llb/IviACPwr IVI Error Converter.vi"/>
				<Item Name="IviACPwr Reset With Defaults.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviacpwr/_IviACPwr.llb/IviACPwr Reset With Defaults.vi"/>
				<Item Name="IviACPwr Reset.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviacpwr/_IviACPwr.llb/IviACPwr Reset.vi"/>
				<Item Name="IviACPwr Revision Query.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviacpwr/_IviACPwr.llb/IviACPwr Revision Query.vi"/>
				<Item Name="IviACPwr Self-Test.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviacpwr/_IviACPwr.llb/IviACPwr Self-Test.vi"/>
				<Item Name="IviCounter Close.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivicounter/_IviCounter.llb/IviCounter Close.vi"/>
				<Item Name="IviCounter Error-Query.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivicounter/_IviCounter.llb/IviCounter Error-Query.vi"/>
				<Item Name="IviCounter Initialize With Options.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivicounter/_IviCounter.llb/IviCounter Initialize With Options.vi"/>
				<Item Name="IviCounter Initialize.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivicounter/_IviCounter.llb/IviCounter Initialize.vi"/>
				<Item Name="IviCounter IVI Error Converter.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivicounter/_IviCounter.llb/IviCounter IVI Error Converter.vi"/>
				<Item Name="IviCounter Reset With Defaults.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivicounter/_IviCounter.llb/IviCounter Reset With Defaults.vi"/>
				<Item Name="IviCounter Reset.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivicounter/_IviCounter.llb/IviCounter Reset.vi"/>
				<Item Name="IviCounter Revision Query.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivicounter/_IviCounter.llb/IviCounter Revision Query.vi"/>
				<Item Name="IviCounter Self-Test.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivicounter/_IviCounter.llb/IviCounter Self-Test.vi"/>
				<Item Name="IviDCPwr Abort [TRG].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividcpwr/_IviDCPwr.llb/IviDCPwr Abort [TRG].vi"/>
				<Item Name="IviDCPwr Close.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividcpwr/_IviDCPwr.llb/IviDCPwr Close.vi"/>
				<Item Name="IviDCPwr Configure Current Limit.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividcpwr/_IviDCPwr.llb/IviDCPwr Configure Current Limit.vi"/>
				<Item Name="IviDCPwr Configure Output Enabled.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividcpwr/_IviDCPwr.llb/IviDCPwr Configure Output Enabled.vi"/>
				<Item Name="IviDCPwr Configure Output Range.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividcpwr/_IviDCPwr.llb/IviDCPwr Configure Output Range.vi"/>
				<Item Name="IviDCPwr Configure OVP.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividcpwr/_IviDCPwr.llb/IviDCPwr Configure OVP.vi"/>
				<Item Name="IviDCPwr Configure Trigger Source [TRG].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividcpwr/_IviDCPwr.llb/IviDCPwr Configure Trigger Source [TRG].vi"/>
				<Item Name="IviDCPwr Configure Triggered Current Limit [TRG].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividcpwr/_IviDCPwr.llb/IviDCPwr Configure Triggered Current Limit [TRG].vi"/>
				<Item Name="IviDCPwr Configure Triggered Voltage Level [TRG].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividcpwr/_IviDCPwr.llb/IviDCPwr Configure Triggered Voltage Level [TRG].vi"/>
				<Item Name="IviDCPwr Configure Voltage Level.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividcpwr/_IviDCPwr.llb/IviDCPwr Configure Voltage Level.vi"/>
				<Item Name="IviDCPwr Error-Query.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividcpwr/_IviDCPwr.llb/IviDCPwr Error-Query.vi"/>
				<Item Name="IviDCPwr Get Channel Name.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividcpwr/_IviDCPwr.llb/IviDCPwr Get Channel Name.vi"/>
				<Item Name="IviDCPwr Initialize With Options.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividcpwr/_IviDCPwr.llb/IviDCPwr Initialize With Options.vi"/>
				<Item Name="IviDCPwr Initialize.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividcpwr/_IviDCPwr.llb/IviDCPwr Initialize.vi"/>
				<Item Name="IviDCPwr Initiate [TRG].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividcpwr/_IviDCPwr.llb/IviDCPwr Initiate [TRG].vi"/>
				<Item Name="IviDCPwr IVI Error Converter.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividcpwr/_IviDCPwr.llb/IviDCPwr IVI Error Converter.vi"/>
				<Item Name="IviDCPwr Measure [MSR].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividcpwr/_IviDCPwr.llb/IviDCPwr Measure [MSR].vi"/>
				<Item Name="IviDCPwr Query Max Current Limit.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividcpwr/_IviDCPwr.llb/IviDCPwr Query Max Current Limit.vi"/>
				<Item Name="IviDCPwr Query Max Voltage Level.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividcpwr/_IviDCPwr.llb/IviDCPwr Query Max Voltage Level.vi"/>
				<Item Name="IviDCPwr Query Output State.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividcpwr/_IviDCPwr.llb/IviDCPwr Query Output State.vi"/>
				<Item Name="IviDCPwr Reset Output Protection.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividcpwr/_IviDCPwr.llb/IviDCPwr Reset Output Protection.vi"/>
				<Item Name="IviDCPwr Reset With Defaults.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividcpwr/_IviDCPwr.llb/IviDCPwr Reset With Defaults.vi"/>
				<Item Name="IviDCPwr Reset.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividcpwr/_IviDCPwr.llb/IviDCPwr Reset.vi"/>
				<Item Name="IviDCPwr Revision Query.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividcpwr/_IviDCPwr.llb/IviDCPwr Revision Query.vi"/>
				<Item Name="IviDCPwr Self-Test.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividcpwr/_IviDCPwr.llb/IviDCPwr Self-Test.vi"/>
				<Item Name="IviDCPwr Send Software Trigger [SWT].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividcpwr/_IviDCPwr.llb/IviDCPwr Send Software Trigger [SWT].vi"/>
				<Item Name="IviDmm Abort.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividmm/_ividmm.llb/IviDmm Abort.vi"/>
				<Item Name="IviDmm Close.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividmm/_ividmm.llb/IviDmm Close.vi"/>
				<Item Name="IviDmm Configure AC Bandwidth [AC].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividmm/_ividmm.llb/IviDmm Configure AC Bandwidth [AC].vi"/>
				<Item Name="IviDmm Configure Auto Zero Mode [AZ].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividmm/_ividmm.llb/IviDmm Configure Auto Zero Mode [AZ].vi"/>
				<Item Name="IviDmm Configure Fixed Ref Junction [TC].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividmm/_ividmm.llb/IviDmm Configure Fixed Ref Junction [TC].vi"/>
				<Item Name="IviDmm Configure Frequency Voltage Range [FRQ].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividmm/_ividmm.llb/IviDmm Configure Frequency Voltage Range [FRQ].vi"/>
				<Item Name="IviDmm Configure Meas Complete Dest [MP].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividmm/_ividmm.llb/IviDmm Configure Meas Complete Dest [MP].vi"/>
				<Item Name="IviDmm Configure Measurement.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividmm/_ividmm.llb/IviDmm Configure Measurement.vi"/>
				<Item Name="IviDmm Configure Multi-Point [MP].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividmm/_ividmm.llb/IviDmm Configure Multi-Point [MP].vi"/>
				<Item Name="IviDmm Configure Power Line Frequency [PLF].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividmm/_ividmm.llb/IviDmm Configure Power Line Frequency [PLF].vi"/>
				<Item Name="IviDmm Configure RTD [RTD].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividmm/_ividmm.llb/IviDmm Configure RTD [RTD].vi"/>
				<Item Name="IviDmm Configure Thermistor [THM].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividmm/_ividmm.llb/IviDmm Configure Thermistor [THM].vi"/>
				<Item Name="IviDmm Configure Thermocouple [TC].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividmm/_ividmm.llb/IviDmm Configure Thermocouple [TC].vi"/>
				<Item Name="IviDmm Configure Transducer Type [TMP].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividmm/_ividmm.llb/IviDmm Configure Transducer Type [TMP].vi"/>
				<Item Name="IviDmm Configure Trigger Slope [TS].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividmm/_ividmm.llb/IviDmm Configure Trigger Slope [TS].vi"/>
				<Item Name="IviDmm Configure Trigger.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividmm/_ividmm.llb/IviDmm Configure Trigger.vi"/>
				<Item Name="IviDmm Error-Query.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividmm/_ividmm.llb/IviDmm Error-Query.vi"/>
				<Item Name="IviDmm Fetch Multi-Point [MP].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividmm/_ividmm.llb/IviDmm Fetch Multi-Point [MP].vi"/>
				<Item Name="IviDmm Fetch.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividmm/_ividmm.llb/IviDmm Fetch.vi"/>
				<Item Name="IviDmm Get Aperture Time Info [DI].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividmm/_ividmm.llb/IviDmm Get Aperture Time Info [DI].vi"/>
				<Item Name="IviDmm Get Auto Range Value [ARV].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividmm/_ividmm.llb/IviDmm Get Auto Range Value [ARV].vi"/>
				<Item Name="IviDmm Initialize With Options.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividmm/_ividmm.llb/IviDmm Initialize With Options.vi"/>
				<Item Name="IviDmm Initialize.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividmm/_ividmm.llb/IviDmm Initialize.vi"/>
				<Item Name="IviDmm Initiate.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividmm/_ividmm.llb/IviDmm Initiate.vi"/>
				<Item Name="IviDmm Is Over-Range.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividmm/_ividmm.llb/IviDmm Is Over-Range.vi"/>
				<Item Name="IviDmm IVI Error Converter.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividmm/_ividmm.llb/IviDmm IVI Error Converter.vi"/>
				<Item Name="IviDmm Read Multi-Point [MP].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividmm/_ividmm.llb/IviDmm Read Multi-Point [MP].vi"/>
				<Item Name="IviDmm Read.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividmm/_ividmm.llb/IviDmm Read.vi"/>
				<Item Name="IviDmm Reset With Defaults.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividmm/_ividmm.llb/IviDmm Reset With Defaults.vi"/>
				<Item Name="IviDmm Reset.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividmm/_ividmm.llb/IviDmm Reset.vi"/>
				<Item Name="IviDmm Revision Query.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividmm/_ividmm.llb/IviDmm Revision Query.vi"/>
				<Item Name="IviDmm Self-Test.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividmm/_ividmm.llb/IviDmm Self-Test.vi"/>
				<Item Name="IviDmm Send Software Trigger [SWT].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ividmm/_ividmm.llb/IviDmm Send Software Trigger [SWT].vi"/>
				<Item Name="IviFgen Abort Generation.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Abort Generation.vi"/>
				<Item Name="IviFgen Clear Arbitrary Memory [SEQ].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Clear Arbitrary Memory [SEQ].vi"/>
				<Item Name="IviFgen Clear Arbitrary Sequence [SEQ].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Clear Arbitrary Sequence [SEQ].vi"/>
				<Item Name="IviFgen Clear Arbitrary Waveform [ARB].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Clear Arbitrary Waveform [ARB].vi"/>
				<Item Name="IviFgen Close.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Close.vi"/>
				<Item Name="IviFgen Configure Advance Trigger [AT].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Configure Advance Trigger [AT].vi"/>
				<Item Name="IviFgen Configure AM Enabled [AM].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Configure AM Enabled [AM].vi"/>
				<Item Name="IviFgen Configure AM Internal [AM].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Configure AM Internal [AM].vi"/>
				<Item Name="IviFgen Configure AM Source [AM].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Configure AM Source [AM].vi"/>
				<Item Name="IviFgen Configure Arb Frequency [AF].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Configure Arb Frequency [AF].vi"/>
				<Item Name="IviFgen Configure Arbitrary Sequence [SEQ].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Configure Arbitrary Sequence [SEQ].vi"/>
				<Item Name="IviFgen Configure Arbitrary Waveform [ARB].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Configure Arbitrary Waveform [ARB].vi"/>
				<Item Name="IviFgen Configure Burst Count [BST].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Configure Burst Count [BST].vi"/>
				<Item Name="IviFgen Configure Data Marker [DM].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Configure Data Marker [DM].vi"/>
				<Item Name="IviFgen Configure FM Enabled [FM].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Configure FM Enabled [FM].vi"/>
				<Item Name="IviFgen Configure FM Internal [FM].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Configure FM Internal [FM].vi"/>
				<Item Name="IviFgen Configure FM Source [FM].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Configure FM Source [FM].vi"/>
				<Item Name="IviFgen Configure Internal Trigger Rate [IT].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Configure Internal Trigger Rate [IT].vi"/>
				<Item Name="IviFgen Configure Operation Mode.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Configure Operation Mode.vi"/>
				<Item Name="IviFgen Configure Output Enabled.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Configure Output Enabled.vi"/>
				<Item Name="IviFgen Configure Output Impedance.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Configure Output Impedance.vi"/>
				<Item Name="IviFgen Configure Output Mode.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Configure Output Mode.vi"/>
				<Item Name="IviFgen Configure Ref Clock Source.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Configure Ref Clock Source.vi"/>
				<Item Name="IviFgen Configure Resume Trigger [RT].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Configure Resume Trigger [RT].vi"/>
				<Item Name="IviFgen Configure Sample Clock [SC].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Configure Sample Clock [SC].vi"/>
				<Item Name="IviFgen Configure Sample Clock Output Enabled [SC].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Configure Sample Clock Output Enabled [SC].vi"/>
				<Item Name="IviFgen Configure Sample Rate [ARB].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Configure Sample Rate [ARB].vi"/>
				<Item Name="IviFgen Configure Sparse Marker [SM].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Configure Sparse Marker [SM].vi"/>
				<Item Name="IviFgen Configure Standard Waveform [STD].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Configure Standard Waveform [STD].vi"/>
				<Item Name="IviFgen Configure Start Trigger [STT].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Configure Start Trigger [STT].vi"/>
				<Item Name="IviFgen Configure Stop Trigger [SPT].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Configure Stop Trigger [SPT].vi"/>
				<Item Name="IviFgen Configure Trigger Source [TRG].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Configure Trigger Source [TRG].vi"/>
				<Item Name="IviFgen Create Arbitrary Sequence [SEQ].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Create Arbitrary Sequence [SEQ].vi"/>
				<Item Name="IviFgen Create Arbitrary Waveform [ARB].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Create Arbitrary Waveform [ARB].vi"/>
				<Item Name="IviFgen Create Channel Arbitrary Waveform [ACH].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Create Channel Arbitrary Waveform [ACH].vi"/>
				<Item Name="IviFgen Create Channel Arbitrary Waveform Int16 [AB].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Create Channel Arbitrary Waveform Int16 [AB].vi"/>
				<Item Name="IviFgen Create Channel Arbitrary Waveform Int32 [AB].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Create Channel Arbitrary Waveform Int32 [AB].vi"/>
				<Item Name="IviFgen Disable All Data Markers [DM].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Disable All Data Markers [DM].vi"/>
				<Item Name="IviFgen Disable All Sparse Markers [SM].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Disable All Sparse Markers [SM].vi"/>
				<Item Name="IviFgen Error-Query.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Error-Query.vi"/>
				<Item Name="IviFgen Get Channel Name.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Get Channel Name.vi"/>
				<Item Name="IviFgen Get Data Marker Name [DM].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Get Data Marker Name [DM].vi"/>
				<Item Name="IviFgen Get Sparse Marker Indexes [SM].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Get Sparse Marker Indexes [SM].vi"/>
				<Item Name="IviFgen Get Sparse Marker Name [SM].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Get Sparse Marker Name [SM].vi"/>
				<Item Name="IviFgen Initialize With Options.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Initialize With Options.vi"/>
				<Item Name="IviFgen Initialize.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Initialize.vi"/>
				<Item Name="IviFgen Initiate Generation.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Initiate Generation.vi"/>
				<Item Name="IviFgen IVI Error Converter.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen IVI Error Converter.vi"/>
				<Item Name="IviFgen Query Arb Sequence Capabilities [SEQ].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Query Arb Sequence Capabilities [SEQ].vi"/>
				<Item Name="IviFgen Query Arb Waveform Capabilities [ARB].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Query Arb Waveform Capabilities [ARB].vi"/>
				<Item Name="IviFgen Query Arb Wfm Capabilities 64 [ARB64].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Query Arb Wfm Capabilities 64 [ARB64].vi"/>
				<Item Name="IviFgen Reset With Defaults.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Reset With Defaults.vi"/>
				<Item Name="IviFgen Reset.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Reset.vi"/>
				<Item Name="IviFgen Revision Query.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Revision Query.vi"/>
				<Item Name="IviFgen Self-Test.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Self-Test.vi"/>
				<Item Name="IviFgen Send Software Advance Trigger [AT].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Send Software Advance Trigger [AT].vi"/>
				<Item Name="IviFgen Send Software Hold Trigger [HT].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Send Software Hold Trigger [HT].vi"/>
				<Item Name="IviFgen Send Software Resume Trigger [RT].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Send Software Resume Trigger [RT].vi"/>
				<Item Name="IviFgen Send Software Stop Trigger [SPT].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Send Software Stop Trigger [SPT].vi"/>
				<Item Name="IviFgen Send Software Trigger [TRG].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Send Software Trigger [TRG].vi"/>
				<Item Name="IviFgen Set Sparse Marker Indexes [SM].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivifgen/_ivifgen.llb/IviFgen Set Sparse Marker Indexes [SM].vi"/>
				<Item Name="IviPwrMeter Close.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivipwrmeter/_iviPwrMeter.llb/IviPwrMeter Close.vi"/>
				<Item Name="IviPwrMeter Error-Query.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivipwrmeter/_iviPwrMeter.llb/IviPwrMeter Error-Query.vi"/>
				<Item Name="IviPwrMeter Initialize With Options.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivipwrmeter/_iviPwrMeter.llb/IviPwrMeter Initialize With Options.vi"/>
				<Item Name="IviPwrMeter Initialize.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivipwrmeter/_iviPwrMeter.llb/IviPwrMeter Initialize.vi"/>
				<Item Name="IviPwrMeter IVI Error Converter.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivipwrmeter/_iviPwrMeter.llb/IviPwrMeter IVI Error Converter.vi"/>
				<Item Name="IviPwrMeter Reset With Defaults.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivipwrmeter/_iviPwrMeter.llb/IviPwrMeter Reset With Defaults.vi"/>
				<Item Name="IviPwrMeter Reset.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivipwrmeter/_iviPwrMeter.llb/IviPwrMeter Reset.vi"/>
				<Item Name="IviPwrMeter Revision Query.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivipwrmeter/_iviPwrMeter.llb/IviPwrMeter Revision Query.vi"/>
				<Item Name="IviPwrMeter Self-Test.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/ivipwrmeter/_iviPwrMeter.llb/IviPwrMeter Self-Test.vi"/>
				<Item Name="IviScope Abort.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Abort.vi"/>
				<Item Name="IviScope Acquisition Status.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Acquisition Status.vi"/>
				<Item Name="IviScope Actual Record Length.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Actual Record Length.vi"/>
				<Item Name="IviScope Actual Sample Mode [SM].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Actual Sample Mode [SM].vi"/>
				<Item Name="IviScope Actual Sample Rate.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Actual Sample Rate.vi"/>
				<Item Name="IviScope Auto Probe Sense Value [PAS].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Auto Probe Sense Value [PAS].vi"/>
				<Item Name="IviScope Auto Setup [AS].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Auto Setup [AS].vi"/>
				<Item Name="IviScope Close.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Close.vi"/>
				<Item Name="IviScope Configure AC Line Trigger Slope [AT].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Configure AC Line Trigger Slope [AT].vi"/>
				<Item Name="IviScope Configure Acquisition Record.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Configure Acquisition Record.vi"/>
				<Item Name="IviScope Configure Acquisition Type.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Configure Acquisition Type.vi"/>
				<Item Name="IviScope Configure Channel Characteristics.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Configure Channel Characteristics.vi"/>
				<Item Name="IviScope Configure Channel.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Configure Channel.vi"/>
				<Item Name="IviScope Configure Edge Trigger Source.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Configure Edge Trigger Source.vi"/>
				<Item Name="IviScope Configure Glitch Trigger Source [GT].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Configure Glitch Trigger Source [GT].vi"/>
				<Item Name="IviScope Configure Initiate Continuous [CA].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Configure Initiate Continuous [CA].vi"/>
				<Item Name="IviScope Configure Interpolation [I].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Configure Interpolation [I].vi"/>
				<Item Name="IviScope Configure Number of Averages [AA].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Configure Number of Averages [AA].vi"/>
				<Item Name="IviScope Configure Number of Envelopes [MmW].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Configure Number of Envelopes [MmW].vi"/>
				<Item Name="IviScope Configure Reference Levels [WM].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Configure Reference Levels [WM].vi"/>
				<Item Name="IviScope Configure Runt Trigger Source [RT].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Configure Runt Trigger Source [RT].vi"/>
				<Item Name="IviScope Configure Trigger Coupling.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Configure Trigger Coupling.vi"/>
				<Item Name="IviScope Configure Trigger Modifier [TM].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Configure Trigger Modifier [TM].vi"/>
				<Item Name="IviScope Configure Trigger.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Configure Trigger.vi"/>
				<Item Name="IviScope Configure TV Trigger Line Number [TV].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Configure TV Trigger Line Number [TV].vi"/>
				<Item Name="IviScope Configure TV Trigger Source [TV].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Configure TV Trigger Source [TV].vi"/>
				<Item Name="IviScope Configure Width Trigger Source [WT].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Configure Width Trigger Source [WT].vi"/>
				<Item Name="IviScope Error-Query.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Error-Query.vi"/>
				<Item Name="IviScope Fetch Min Max Waveform [MmW].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Fetch Min Max Waveform [MmW].vi"/>
				<Item Name="IviScope Fetch Waveform Measurement [WM].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Fetch Waveform Measurement [WM].vi"/>
				<Item Name="IviScope Fetch Waveform.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Fetch Waveform.vi"/>
				<Item Name="IviScope Get Channel Name.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Get Channel Name.vi"/>
				<Item Name="IviScope Initialize With Options.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Initialize With Options.vi"/>
				<Item Name="IviScope Initialize.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Initialize.vi"/>
				<Item Name="IviScope Initiate Acquisition.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Initiate Acquisition.vi"/>
				<Item Name="IviScope Invalidate All Attributes.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Invalidate All Attributes.vi"/>
				<Item Name="IviScope Is Invalid Waveform Element.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Is Invalid Waveform Element.vi"/>
				<Item Name="IviScope IVI Error Converter.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope IVI Error Converter.vi"/>
				<Item Name="IviScope Read Min Max Waveform [MmW].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Read Min Max Waveform [MmW].vi"/>
				<Item Name="IviScope Read Waveform Measurement [WM].vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Read Waveform Measurement [WM].vi"/>
				<Item Name="IviScope Read Waveform.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Read Waveform.vi"/>
				<Item Name="IviScope Reset With Defaults.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Reset With Defaults.vi"/>
				<Item Name="IviScope Reset.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Reset.vi"/>
				<Item Name="IviScope Revision Query.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Revision Query.vi"/>
				<Item Name="IviScope Self-Test.vi" Type="VI" URL="/&lt;vilib&gt;/ivi/iviscope/_iviscope.llb/IviScope Self-Test.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVDateTimeRec.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVDateTimeRec.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="MoveMemory.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/MoveMemory.vi"/>
				<Item Name="NET_convertLocalhostURLToMachineURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_convertLocalhostURLToMachineURL.vi"/>
				<Item Name="NET_GetHostName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_GetHostName.vi"/>
				<Item Name="NET_handleDotInTagName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_handleDotInTagName.vi"/>
				<Item Name="NET_IsComputerLocalhost.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_IsComputerLocalhost.vi"/>
				<Item Name="NET_localhostToMachineName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_localhostToMachineName.vi"/>
				<Item Name="NET_resolveNVIORef.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_resolveNVIORef.vi"/>
				<Item Name="NET_resolveTagURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_resolveTagURL.vi"/>
				<Item Name="NET_tagURLdecode.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_tagURLdecode.vi"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="ni_citadel_lv.dll" Type="Document" URL="/&lt;vilib&gt;/citadel/ni_citadel_lv.dll"/>
				<Item Name="NI_DSC.lvlib" Type="Library" URL="/&lt;vilib&gt;/lvdsc/NI_DSC.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="ni_logos_BuildURL.vi" Type="VI" URL="/&lt;vilib&gt;/variable/logos/dll/ni_logos_BuildURL.vi"/>
				<Item Name="ni_logos_ValidatePSPItemName.vi" Type="VI" URL="/&lt;vilib&gt;/variable/logos/dll/ni_logos_ValidatePSPItemName.vi"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_SystemLogging.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/SystemLogging/NI_SystemLogging.lvlib"/>
				<Item Name="ni_tagger_lv_NewFolder.vi" Type="VI" URL="/&lt;vilib&gt;/variable/tagger/ni_tagger_lv_NewFolder.vi"/>
				<Item Name="ni_tagger_lv_ReadVariableConfig.vi" Type="VI" URL="/&lt;vilib&gt;/variable/tagger/ni_tagger_lv_ReadVariableConfig.vi"/>
				<Item Name="NI_Variable.lvlib" Type="Library" URL="/&lt;vilib&gt;/variable/NI_Variable.lvlib"/>
				<Item Name="nialarms.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/nialarms.dll"/>
				<Item Name="Not A Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Not A Semaphore.vi"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Obtain Semaphore Reference.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Obtain Semaphore Reference.vi"/>
				<Item Name="Open Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Open Registry Key.vi"/>
				<Item Name="Open_Create_Replace File.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/Open_Create_Replace File.vi"/>
				<Item Name="ParseXMLFragments.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/ParseXMLFragments.vi"/>
				<Item Name="PRC_AdoptVarBindURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_AdoptVarBindURL.vi"/>
				<Item Name="PRC_CachedLibVariables.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CachedLibVariables.vi"/>
				<Item Name="PRC_CommitMultiple.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CommitMultiple.vi"/>
				<Item Name="PRC_ConvertDBAttr.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_ConvertDBAttr.vi"/>
				<Item Name="PRC_CreateFolders.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateFolders.vi"/>
				<Item Name="PRC_CreateProc.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateProc.vi"/>
				<Item Name="PRC_CreateSubLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateSubLib.vi"/>
				<Item Name="PRC_CreateVar.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateVar.vi"/>
				<Item Name="PRC_DataType2Prototype.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DataType2Prototype.vi"/>
				<Item Name="PRC_DeleteLibraryItems.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DeleteLibraryItems.vi"/>
				<Item Name="PRC_DeleteProc.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DeleteProc.vi"/>
				<Item Name="PRC_Deploy.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_Deploy.vi"/>
				<Item Name="PRC_DumpProcess.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DumpProcess.vi"/>
				<Item Name="PRC_DumpSharedVariables.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DumpSharedVariables.vi"/>
				<Item Name="PRC_EnableAlarmLogging.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_EnableAlarmLogging.vi"/>
				<Item Name="PRC_EnableDataLogging.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_EnableDataLogging.vi"/>
				<Item Name="PRC_GetLibFromURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetLibFromURL.vi"/>
				<Item Name="PRC_GetMonadAttr.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetMonadAttr.vi"/>
				<Item Name="PRC_GetMonadList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetMonadList.vi"/>
				<Item Name="PRC_GetProcList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetProcList.vi"/>
				<Item Name="PRC_GetProcSettings.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetProcSettings.vi"/>
				<Item Name="PRC_GetVarAndSubLibs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetVarAndSubLibs.vi"/>
				<Item Name="PRC_GetVarList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetVarList.vi"/>
				<Item Name="PRC_GroupSVs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GroupSVs.vi"/>
				<Item Name="PRC_IOServersToLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_IOServersToLib.vi"/>
				<Item Name="PRC_MakeFullPathWithCurrentVIsCallerPath.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_MakeFullPathWithCurrentVIsCallerPath.vi"/>
				<Item Name="PRC_MutipleDeploy.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_MutipleDeploy.vi"/>
				<Item Name="PRC_OpenOrCreateLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_OpenOrCreateLib.vi"/>
				<Item Name="PRC_ParseLogosURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_ParseLogosURL.vi"/>
				<Item Name="PRC_ROSProc.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_ROSProc.vi"/>
				<Item Name="PRC_SVsToLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_SVsToLib.vi"/>
				<Item Name="PRC_Undeploy.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_Undeploy.vi"/>
				<Item Name="PSP Enumerate Network Items.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/tagapi/internal/PSP Enumerate Network Items.vi"/>
				<Item Name="PTH_ConstructCustomURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/path/PTH_ConstructCustomURL.vi"/>
				<Item Name="PTH_EmptyOrNotAPath.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/path/PTH_EmptyOrNotAPath.vi"/>
				<Item Name="PTH_IsUNC.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/path/PTH_IsUNC.vi"/>
				<Item Name="Read From XML File(array).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File(array).vi"/>
				<Item Name="Read From XML File(string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File(string).vi"/>
				<Item Name="Read From XML File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File.vi"/>
				<Item Name="Read Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value DWORD.vi"/>
				<Item Name="Read Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple STR.vi"/>
				<Item Name="Read Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple U32.vi"/>
				<Item Name="Read Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple.vi"/>
				<Item Name="Read Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value STR.vi"/>
				<Item Name="Read Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value.vi"/>
				<Item Name="Registry Handle Master.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Handle Master.vi"/>
				<Item Name="Registry refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry refnum.ctl"/>
				<Item Name="Registry RtKey.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry RtKey.ctl"/>
				<Item Name="Registry SAM.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry SAM.ctl"/>
				<Item Name="Registry Simplify Data Type.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Simplify Data Type.vi"/>
				<Item Name="Registry View.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry View.ctl"/>
				<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry WinErr-LVErr.vi"/>
				<Item Name="Release Semaphore Reference.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Release Semaphore Reference.vi"/>
				<Item Name="Release Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Release Semaphore.vi"/>
				<Item Name="RemoveNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/RemoveNamedSemaphorePrefix.vi"/>
				<Item Name="Report Error Msg.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/ActorFramework/Report Error Msg/Report Error Msg.lvclass"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="SEC Get Interactive User.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/custom/SEC Get Interactive User.vi"/>
				<Item Name="Semaphore RefNum" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore RefNum"/>
				<Item Name="Semaphore Refnum Core.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore Refnum Core.ctl"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Busy.vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/STR_ASCII-Unicode.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="Subscribe All Local Processes.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/controls/Alarms and Events/internal/Subscribe All Local Processes.vi"/>
				<Item Name="subTimeDelay.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/TimeDelayBlock.llb/subTimeDelay.vi"/>
				<Item Name="Syslog Collector Close.vi" Type="VI" URL="/&lt;vilib&gt;/NI/syslog/Syslog Collector Close.vi"/>
				<Item Name="Syslog Collector Init.vi" Type="VI" URL="/&lt;vilib&gt;/NI/syslog/Syslog Collector Init.vi"/>
				<Item Name="Syslog Collector Read.vi" Type="VI" URL="/&lt;vilib&gt;/NI/syslog/Syslog Collector Read.vi"/>
				<Item Name="Syslog Device Close.vi" Type="VI" URL="/&lt;vilib&gt;/NI/syslog/Syslog Device Close.vi"/>
				<Item Name="Syslog Device Init.vi" Type="VI" URL="/&lt;vilib&gt;/NI/syslog/Syslog Device Init.vi"/>
				<Item Name="Syslog Device Send.vi" Type="VI" URL="/&lt;vilib&gt;/NI/syslog/Syslog Device Send.vi"/>
				<Item Name="syslog_Device Function Engine.vi" Type="VI" URL="/&lt;vilib&gt;/NI/syslog/_subVIs/syslog_Device Function Engine.vi"/>
				<Item Name="syslog_device_functions.ctl" Type="VI" URL="/&lt;vilib&gt;/NI/syslog/_subVIs/TypeDefs/syslog_device_functions.ctl"/>
				<Item Name="syslog_facility_codes.ctl" Type="VI" URL="/&lt;vilib&gt;/NI/syslog/_subVIs/TypeDefs/syslog_facility_codes.ctl"/>
				<Item Name="syslog_Hostname.vi" Type="VI" URL="/&lt;vilib&gt;/NI/syslog/_subVIs/syslog_Hostname.vi"/>
				<Item Name="syslog_Message Parse.vi" Type="VI" URL="/&lt;vilib&gt;/NI/syslog/_subVIs/syslog_Message Parse.vi"/>
				<Item Name="syslog_message_cluster.ctl" Type="VI" URL="/&lt;vilib&gt;/NI/syslog/_subVIs/TypeDefs/syslog_message_cluster.ctl"/>
				<Item Name="syslog_severity_codes.ctl" Type="VI" URL="/&lt;vilib&gt;/NI/syslog/_subVIs/TypeDefs/syslog_severity_codes.ctl"/>
				<Item Name="syslog_Timestamp.vi" Type="VI" URL="/&lt;vilib&gt;/NI/syslog/_subVIs/syslog_Timestamp.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
				<Item Name="Time-Delayed Send Message Core.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message Core.vi"/>
				<Item Name="Time-Delayed Send Message.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message.vi"/>
				<Item Name="TIME_FormatTS(TS).vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/time/TIME_FormatTS(TS).vi"/>
				<Item Name="TIME_StartTsLEStopTs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/time/TIME_StartTsLEStopTs.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Unset Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Unset Busy.vi"/>
				<Item Name="usereventprio.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/usereventprio.ctl"/>
				<Item Name="util_Buffer Strings.vi" Type="VI" URL="/&lt;vilib&gt;/NI/syslog/examples/util/util_Buffer Strings.vi"/>
				<Item Name="util_My IP Address.vi" Type="VI" URL="/&lt;vilib&gt;/NI/syslog/examples/util/util_My IP Address.vi"/>
				<Item Name="Validate Semaphore Size.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Validate Semaphore Size.vi"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="VerQueryValue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/VerQueryValue.vi"/>
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
				<Item Name="VISA Lock Async.vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Lock Async.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="WINMEM_Get Avail Virtual Memory.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/platform/win32/WINMEM_Get Avail Virtual Memory.vi"/>
				<Item Name="WINMEM_Get Disk Free Space.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/platform/win32/WINMEM_Get Disk Free Space.vi"/>
				<Item Name="WINMEM_Global Memory Status.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/platform/win32/WINMEM_Global Memory Status.vi"/>
				<Item Name="Write Spreadsheet String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Spreadsheet String.vi"/>
				<Item Name="Write To Spreadsheet File (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File (DBL).vi"/>
				<Item Name="Write To Spreadsheet File (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File (I64).vi"/>
				<Item Name="Write To Spreadsheet File (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File (string).vi"/>
				<Item Name="Write To Spreadsheet File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File.vi"/>
				<Item Name="Write to XML File(array).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File(array).vi"/>
				<Item Name="Write to XML File(string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File(string).vi"/>
				<Item Name="Write to XML File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File.vi"/>
			</Item>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="ivi.dll" Type="Document" URL="ivi.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="IviACPwr_ni.dll" Type="Document" URL="IviACPwr_ni.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="IviCounter_ni.dll" Type="Document" URL="IviCounter_ni.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="ividcpwr.dll" Type="Document" URL="ividcpwr.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="IviDmm.dll" Type="Document" URL="IviDmm.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="IviFgen.dll" Type="Document" URL="IviFgen.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="IviPwrMeter.dll" Type="Document" URL="IviPwrMeter.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="IviScope.dll" Type="Document" URL="IviScope.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="lksock.dll" Type="Document" URL="lksock.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="logosbrw.dll" Type="Document" URL="/&lt;resource&gt;/logosbrw.dll"/>
			<Item Name="LV Config Read String.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read String.vi"/>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="nilvaiu.dll" Type="Document" URL="nilvaiu.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="nitaglv.dll" Type="Document" URL="nitaglv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="NVIORef.dll" Type="Document" URL="NVIORef.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="SCT Default Types.ctl" Type="VI" URL="/&lt;resource&gt;/dialog/variable/SCT Default Types.ctl"/>
			<Item Name="SCT Get LVRTPath.vi" Type="VI" URL="/&lt;resource&gt;/dialog/variable/SCT Get LVRTPath.vi"/>
			<Item Name="SCT Get Types.vi" Type="VI" URL="/&lt;resource&gt;/dialog/variable/SCT Get Types.vi"/>
			<Item Name="systemLogging.dll" Type="Document" URL="systemLogging.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="version.dll" Type="Document" URL="version.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="CSPP DIM Application" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{BBEEE137-3ECF-4992-9BFB-27E498DF6B70}</Property>
				<Property Name="App_INI_GUID" Type="Str">{D076C128-BC42-4A9B-B136-D29334111D9E}</Property>
				<Property Name="App_INI_itemID" Type="Ref"></Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_useFFRTE" Type="Bool">true</Property>
				<Property Name="App_waitDebugging" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{D49593D3-88B1-4450-9C1E-F9C2CE4AE99A}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">Demo Build Specification</Property>
				<Property Name="Bld_buildSpecName" Type="Str">CSPP DIM Application</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/CSPP/CSPP DIM Application</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{F3DA460C-BFFF-460B-ABEE-0F487BB46015}</Property>
				<Property Name="Bld_supportedLanguage[0]" Type="Str">English</Property>
				<Property Name="Bld_supportedLanguageCount" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Bld_version.minor" Type="Int">2</Property>
				<Property Name="Destination[0].destName" Type="Str">CSPP-DIM.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/CSPP/CSPP DIM Application/CSPP-DIM.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/CSPP/CSPP DIM Application/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Exe_cmdLineArgs" Type="Bool">true</Property>
				<Property Name="Exe_iconItemID" Type="Ref">/My Computer/CS++.ico</Property>
				<Property Name="Source[0].itemID" Type="Str">{5374EE65-B6AA-4698-8312-2285383DAF96}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Packages/CSPP_Core/Actors/CS++StartActor.lvlib/Launch CS++StartActor.vi</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[10].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[10].itemID" Type="Ref">/My Computer/lib.lib/LVDimInterface.lvlib/supportFiles/myDimStd.dll</Property>
				<Property Name="Source[10].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[11].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[11].itemID" Type="Ref">/My Computer/lib.lib/LVDimInterface.lvlib/supportFiles/myDimSPL.dll</Property>
				<Property Name="Source[11].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[12].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[12].itemID" Type="Ref">/My Computer/CSPP_Main.vi</Property>
				<Property Name="Source[12].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[12].type" Type="Str">VI</Property>
				<Property Name="Source[2].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/EUPL License</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].type" Type="Str">Container</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref"></Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref"></Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/Dependencies/Items in Memory/LVDimInterface.lvlib/public/common/LVDimInterface.dim_operate.vi</Property>
				<Property Name="Source[5].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[5].type" Type="Str">VI</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/Dependencies/Items in Memory/LVDimInterface.lvlib/supportFiles/libDimWrapper.dll</Property>
				<Property Name="Source[6].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/Dependencies/Items in Memory/LVDimInterface.lvlib/supportFiles/libDimWrapperSPL.dll</Property>
				<Property Name="Source[7].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/Dependencies/Items in Memory/LVDimInterface.lvlib/supportFiles/msvcp100.dll</Property>
				<Property Name="Source[8].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[9].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[9].itemID" Type="Ref">/My Computer/Dependencies/Items in Memory/LVDimInterface.lvlib/supportFiles/msvcr100.dll</Property>
				<Property Name="Source[9].sourceInclusion" Type="Str">Include</Property>
				<Property Name="SourceCount" Type="Int">13</Property>
				<Property Name="TgtF_companyName" Type="Str">GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_fileDescription" Type="Str">CSPP Demo Application</Property>
				<Property Name="TgtF_internalName" Type="Str">CSPP DIM Application</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2013 GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_productName" Type="Str">CSPP DIM Application</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{E09FD4F5-C689-468F-9DD7-98AAD66B72B2}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">CSPP-DIM.exe</Property>
			</Item>
		</Item>
	</Item>
</Project>
